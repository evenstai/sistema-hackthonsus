import { InserviceService } from './../../services/inservice.service';
import { ResponsesModuleService } from './../../services/modules_services/responses_module.service';
import { ErrorInputService } from 'src/services/modules_services/error_input.service';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';
import { FormGroup, FormControl } from '@angular/forms'

import { Cadastro } from '../../models/cadastro.model'

import { Auth } from '../../services/auth.service'
import { Requests } from 'src/services/requests.service';
import { ImagemModuleService } from 'src/services/modules_services/enviar_imagem.service';
import { TelefoneValidation } from 'src/utils/valida-telefone';
import { ValidaCnpj } from 'src/utils/valida-cnpj';
import { ValidaCpf } from 'src/utils/valida-cpf';
declare var $: any;

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css'],
  animations: [
    trigger('p1', [
      state('escondido', style({
        opacity: 0
      })),
        
      state('visivel', style({
        opacity: 1
      })),
      transition('void => visivel',[
        style({opacity: 0}),
        animate('0.4s ease-in', keyframes([
          style({ offset: 0.12, opacity: 0}),
          style({ offset: 0.90, opacity: 1})
        ]))
      ]),
      transition('escondido => visivel',[
        style({opacity: 0}),
        animate('0.5s ease-in-out')
      ]),
      transition('visivel => escondido',[
        style({opacity: 1}),
        animate('0.5s ease-in-out')
      ])
    ])
  ],
  providers: [
    ErrorInputService, 
    ResponsesModuleService,
    ImagemModuleService,
    TelefoneValidation,
    ValidaCnpj,
    ValidaCpf]
})
export class CadastroComponent implements OnInit {

  @Output() public exibirPainel: EventEmitter<string> = new EventEmitter()

  public formCadastro: FormGroup = new FormGroup({
    'nome_e_sobrenome' : new FormControl(null),
    'email' : new FormControl(null),
    'celular' : new FormControl(null),
    'cpf' : new FormControl(null),
    'cnpj' : new FormControl(null),
    'razao_social' : new FormControl(null),
    'fantasia' : new FormControl(null),
    'senha' : new FormControl(null),
    're_senha' : new FormControl(null),
    'cep' : new FormControl(null),
    'endereco' : new FormControl(null),
    'numero' : new FormControl(null),
    'bairro' : new FormControl(null),
    'cidade' : new FormControl(null),
    'estado' : new FormControl(null),
    'codigo' : new FormControl(null)
  })
  
  public cep_mask = [/\d/,/\d/,/\d/,/\d/,/\d/, '-', /\d/, /\d/, /\d/]
  public cnpj_mask = [/\d/,/\d/,'.',/\d/,/\d/,/\d/,'.',/\d/,/\d/,/\d/, '/', /\d/, /\d/, /\d/, /\d/,'-',/\d/,/\d/,]
  public celular_mask = ['(', /\d/, /\d/,')', /\d/, /\d/,/\d/,/\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public cpf_mask = [/\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '-', /\d/, /\d/]

  public nome : boolean
  public celular : boolean
  public email : boolean
  public cnpjValido : boolean
  public cpfValido : boolean
  public razao_social : boolean
  public fantasia : boolean
  public senha : boolean
  public re_senha : boolean
  public cep : boolean
  public endereco : boolean
  public numero : boolean
  public bairro : boolean
  public cidade : boolean
  public estado : boolean
  public codigo : boolean

  public img_base64 : string = ""
  public img_matriz : string = "/assets/new_user_img.svg"

  public choice :boolean = true;
  public PL: boolean = false;
  public PJ: boolean = false;
  public choicedPl: boolean = true;
  public choicedPj: boolean = true;
  public p1 :boolean = false;
  public p2PJ :boolean = false;
  public p2 :boolean = false;
  public p3 :boolean = false;
  public p4 :boolean = false;
  public p5 :boolean = false;

  public estadoVoltar: string = "escondido"
  public estadoChoice: string = "visivel"
  public estadoInfo: string = "visivel"
  public estadoLoad: boolean = false
  public estadoLoadProposta : boolean = false
  public estadoP1: string = "visivel"
  public estadoPJP2: string = "visível"
  public estadoP2: string = "visivel"
  public estadoP3: string = "visivel"
  public estadoP4: string = "visivel"
  public estadoP5: string = "visivel"
  public error: string

  public estadoFormP1 : string = 'disabled'
  public estadoFormP2 : string = 'disabled'
  public estadoFormP3 : string = 'disabled'
  public estadoFormP4 : string = 'disabled'
  public estadoFormP5 : string = 'disabled'

  public vencimento_gratis : string

  public mensagem_loader : string
  public response_proposta : any
  public propostaScreen : boolean = true
  public responsePropostaScreen : boolean = false
  public responseScreen : boolean = false
  public errors_input : boolean = false
  public codigo_proposta : string
  public valor_mensalidade : number
  public propostaForm : FormGroup = new FormGroup({
    'codigo' : new FormControl(null)
  })
  
  constructor(
    private auth: Auth,
    private errorMessageService : ErrorInputService,
    private responsesModuleService : ResponsesModuleService,
    private requests : Requests,
    private validaTelef : TelefoneValidation,
    private validaCnpj : ValidaCnpj,
    private validaCpf : ValidaCpf,
    private serv: InserviceService
  ) { }

  ngOnInit() {
    ImagemModuleService.emitirImagemBase64.subscribe(
      imagem => {
        this.img_base64 = imagem
        this.img_matriz = imagem
        $('#exampleModall').modal('hide');
      }
    )

    ResponsesModuleService.emitirFecharResponseModule.subscribe(
      trigger => {
        $('#modalResponse').modal('hide');
        this.responseScreen = false
      }
    )
    
  }

  consultaCep(){
    let cep = this.formCadastro.get('cep').value
    let novocep = cep.replace('-','')
    if(cep != ''){
      var validacep = /^[0-9]{8}$/;
      if(validacep.test(novocep)){

        this.serv.buscaCep(novocep).subscribe(dados => {
          
          //console.log(dados)
          this.formCadastro.get('bairro').setValue(dados.bairro)
          this.formCadastro.get('endereco').setValue(dados.logradouro)
          this.formCadastro.get('estado').setValue(dados.uf)
          this.formCadastro.get('cidade').setValue(dados.localidade)
          this.validaBairro()
          this.validaRua()
          this.validaCidade()
          this.validaEstado()
        })

      }
    }
  }

  public exibirSomentePL(){
    this.PL = true
    this.PJ = false
    this.choicedPj= false;
  }

  public exibirSomentePJ(){
    this.PL = false
    this.PJ = true
    this.choicedPl= false;
  }

  public exibirChoice(){
    this.estadoVoltar = "escondido"
    this.estadoChoice = "visivel"
    this.choice = true
    this.choicedPj= true;
    this.choicedPl= true;
    this.PL = false
    this.PJ = false
    this.p1 = false
    this.p2 = false
    this.p3 = false
    this.p4 = false
    this.p5 = false
  }

  public exibirCadastroP1(): void {
    this.estadoVoltar = "visivel"
    this.estadoChoice = "escondido"
    this.choice = false
    this.p1 = true
    this.p2PJ = false
    this.p2 = false
    this.p3 = false
    this.p4 = false
    this.p5 = false
  }

  public exibirCadastroP2(): void {
    if(this.p2PJ){
      this.estadoVoltar = "visivel"
        this.p1 = false
        this.p2PJ = false
        this.p2 = true
        this.p3 = false
        this.p4 = false
        this.p5 = false
    }else{
      if(this.PJ){
        this.estadoVoltar = "visivel"
        this.p1 = false
        this.p2PJ = true
        this.p2 = false
        this.p3 = false
        this.p4 = false
        this.p5 = false
      }else if(this.PL){
        this.estadoVoltar = "visivel"
        this.p1 = false
        this.p2PJ = false
        this.p2 = true
        this.p3 = false
        this.p4 = false
        this.p5 = false
      }
    }
  }

  public exibirCadastroP3(): void {
    this.estadoVoltar = "visivel"
    this.p1 = false
    this.p2PJ = false
    this.p2 = false
    this.p3 = true
    this.p4 = false
    this.p5 = false
  }

  public exibirCadastroP4(): void {
    this.estadoVoltar = "visivel"
    this.p1 = false
    this.p2 = false
    this.p3 = false
    this.p4 = true
    this.p5 = false
  }

  public exibirCadastroP5(): void {
    this.estadoVoltar = "visivel"
    this.p1 = false
    this.p2 = false
    this.p3 = false
    this.p4 = false
    this.p5 = true
  }


  public voltarCadastro(): void {
    if(this.p1 && this.p2 == false && this.p3 == false  && this.p4 == false && this.p5 == false){
      this.exibirChoice()
    }
    else if(this.p1 == false && this.p2 && this.p3 == false && this.p4 == false && this.p5 == false){
      if(this.PJ){
        this.exibirCadastroP2()
      }else if(this.PL){
        this.exibirCadastroP1()
      }
    }
    else if(this.p1 == false && this.p2PJ && this.p3 == false && this.p4 == false && this.p5 == false){
      this.exibirCadastroP1()
    }
    else if(this.p1 == false && this.p2 == false && this.p3 && this.p4 == false && this.p5 == false){
      this.exibirCadastroP2()
    }
    else if(this.p1 == false && this.p2 == false && this.p3 == false && this.p4 && this.p5 == false){
      this.exibirCadastroP3()
    }
    else if(this.p1 == false && this.p2 == false && this.p3 == false && this.p4 == false && this.p5){
      this.exibirCadastroP4()
    }
  }

  public exibirPainelLogin(): void {
    this.exibirPainel.emit('login')
  }

  public abrirLoad(status :boolean) :void{
      this.estadoLoad = status;
  }

  public cadastrarUsuarioPJ(): void{

    let cadastro: Cadastro = new Cadastro(
      "PJ",
      this.formCadastro.value.nome_e_sobrenome,
      this.formCadastro.value.email,
      this.formCadastro.value.celular,
      this.formCadastro.value.cpf,
      this.formCadastro.value.cnpj,
      this.formCadastro.value.razao_social,
      this.formCadastro.value.fantasia,
      this.formCadastro.value.senha,
      this.formCadastro.value.cep,
      this.formCadastro.value.endereco,
      this.formCadastro.value.numero,
      this.formCadastro.value.bairro,
      this.formCadastro.value.cidade,
      this.formCadastro.value.estado,
      this.formCadastro.value.codigo,
      this.img_base64
    )

    this.abrirLoad(true)

    this.auth.cadastrarEmpresa(cadastro)
    .subscribe(
      data => {
        this.abrirLoad(false)
        var resp_cadastro: any = data

        if(resp_cadastro.code===200){
          this.mostrarResponseScreen(
            true,
            resp_cadastro.code,
            resp_cadastro.mensagem,
            "400px"
          )
        }else{
          this.mostrarResponseScreen(
            true,
            resp_cadastro.code,
            resp_cadastro.data,
            "400px"
          )
        }
      })

    }

    verificaProposta(){

      if(this.formCadastro.value.codigo != null){
        if(this.formCadastro.value.codigo.length>0){
        this.mostrarErrordeInput(false)
        this.mostrarLoaderProposta(true, "Buscando Proposta")
  
        let proposta : any = {
          codigo_proposta: this.formCadastro.value.codigo
        }
        
        this.requests.buscarProposta(proposta)
        .subscribe(
          res => {
            this.mostrarLoaderProposta(false, null)
            var resp_proposta : any = res
            if(resp_proposta!=null){
              if(resp_proposta.code===200){
                this.response_proposta = resp_proposta.data[0]
                this.vencimento_gratis = this.nextMonth()
                this.codigo_proposta = this.formCadastro.value.codigo
                this.valor_mensalidade = this.response_proposta.valor_mensalidade.replace(".",",")
                this.mostrarResponseProposta(true)
              }else{
                this.mostrarResponseScreen(
                  true,
                  resp_proposta.code,
                  resp_proposta.mensagem,
                  "400px"
                )
              }
            }else{
              this.mostrarLoaderProposta(false, null)
            }
          }
        )
        }else{
          this.mostrarErrordeInput(true)
          this.errorMessageService.enviaMensagemErro("Você deve inserir um código de proposta.")
        }
      }else{
        this.mostrarErrordeInput(true)
        this.errorMessageService.enviaMensagemErro("Você deve inserir um código de proposta.")
      }
    }

    pularImagem(){
      this.img_matriz = "/assets/new_user_img.svg"
      this.img_base64 = ""
      this.exibirCadastroP4()
    }

    prosseguirCadastro(){
      this.mostrarResponseProposta(false)
    }

    mostrarLoaderProposta(status: boolean, mensagem: string){
      this.estadoLoadProposta = status
      this.mensagem_loader = mensagem
    }

    mostrarLoader(status: boolean, mensagem: string){
      this.estadoLoad = status
      this.mensagem_loader = mensagem
    }

    mostrarErrordeInput(status :boolean){
      this.errors_input = status
    }
    mostrarResponseScreen(status: boolean, code: number, mensagem: string, altura: string){
      this.responseScreen = status
      this.responsesModuleService.enviaConfigResponse(code, mensagem, altura)
      if(status)$('#modalResponse').modal('show');
    }
  
    mostrarResponseProposta(status: boolean){
      this.responsePropostaScreen = status
    }

    validaNome(){
      if(this.formCadastro.value.nome_e_sobrenome.length<7 || this.formCadastro.value.nome_e_sobrenome.indexOf(" ") == -1){
        this.nome = false
      }else{
        this.nome = true
      }
      this.ativarBotaoP1() 
    }

    validaCelular(){
      this.celular = this.validaTelef.validarTel(this.formCadastro.value.celular)
      this.ativarBotaoP1()
    }

    validaEmail(){
      if(this.formCadastro.value.email.indexOf('@') == -1 || this.formCadastro.value.email.indexOf('.') == -1 ){
        this.email = false
      }else{
        this.email = true
      }
      this.ativarBotaoP1()
    }

    validaCNPJ(cnpj){
      this.cnpjValido = this.validaCnpj.validarCNPJ(cnpj.value)
      this.ativarBotaoP2()
    }

    validaRazaoSocial(){
      if(this.formCadastro.value.razao_social.length>3){
        this.razao_social = true
        
      }else{
        this.razao_social = false
      }
      this.ativarBotaoP2()
    }

    validaNomeFantasia(){
      if(this.formCadastro.value.fantasia.length>3){
        this.fantasia = true
        
      }else{
        this.fantasia = false
      }
      this.ativarBotaoP2()
    }

    validaBairro(){
      if(this.formCadastro.value.bairro.length>4){
        this.bairro = true
        
      }else{
        this.bairro = false
      }
      this.ativarBotaoP3()
    }
  
    validaRua(){
      if(this.formCadastro.value.endereco.length>1){
        this.endereco = true
        
      }else{
        this.endereco = false
      }
      this.ativarBotaoP3()
    }
  
    validaCidade(){
      if(this.formCadastro.value.cidade.length>2){
        this.cidade = true
        
      }else{
        this.cidade = false
      }
      this.ativarBotaoP3()
    }
  
    validaCep(cep){
      cep = this.formCadastro.value.cep.replace("-","")
      var cepFinal = cep.replace(/_/g,"")
        if(cepFinal.toString().length == 8){
          this.cep = true
        }else{
          this.cep = false
        }
        this.ativarBotaoP3()
    }
  
  
    validaEstado(){
      if(this.formCadastro.value.estado.length == 2){
        this.estado = true
        
      }else{
        this.estado = false
      }
      this.ativarBotaoP3()
    }
  
    validaNumero(){
      if(this.formCadastro.value.numero>0){
        this.numero = true
      }else{
        this.numero = false
      }
      this.ativarBotaoP3()
    }

    validaCodigo(){
      if(this.formCadastro.value.codigo.length>5){
        this.codigo = true
        
      }else{
        this.codigo = false
      }
      this.ativarBotaoP4()
    }

    validaCPF(cpf){
      this.cpfValido = this.validaCpf.validarCPF(cpf.value)
      this.ativarBotaoP5()
    }

    validaSenha(){
      if(this.formCadastro.value.senha.length>7){
        this.senha = true
      }else{
        this.senha = false
      }
      this.ativarBotaoP5()
    }
  
    validaNovaSenha(){
      if(this.formCadastro.value.re_senha.length>7){
        this.re_senha = true
      }else{
        this.re_senha = false
      }
      this.ativarBotaoP5()
    }

    ativarBotaoP1(){
      if(this.nome && this.celular && this.email){
        this.estadoFormP1 = ''
      }else{
        this.estadoFormP1 = 'disabled'
      }
    }

    ativarBotaoP2(){
      if(this.cnpjValido && this.razao_social && this.fantasia){
        this.estadoFormP2 = ''
      }else{
        this.estadoFormP2 = 'disabled'
      }
    }

    ativarBotaoP3(){
      if(this.cep && this.endereco && this.numero && this.bairro && this.cidade && this.estado){
        this.estadoFormP3 = ''
      }else{
        this.estadoFormP3 = 'disabled'
      }
    }

    ativarBotaoP4(){
      if(this.codigo){
        this.estadoFormP4 = ''
      }else{
        this.estadoFormP4 = 'disabled'
      }
    }

    ativarBotaoP5(){
      if(this.cpfValido && this.senha && this.re_senha){
        this.estadoFormP5 = ''
      }else{
        this.estadoFormP5 = 'disabled'
      }
    }

    nextMonth(){
      var today = new Date
      var dia = today.getDate()
      var mes = today.getMonth()+1
      var ano = today.getFullYear()
  
      if(mes>12){
        mes = 12
      }
  
      if(dia>9){
        var vencimento_final = dia+"/"+mes+"/"+ano
      }else{
        var vencimento_final = "0"+dia+"/"+mes+"/"+ano
      }
  
      return vencimento_final
    }

}
