import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate} from '@angular/animations';
import { AcessoService } from 'src/services/modules_services/acesso.service';
import { Auth } from 'src/services/auth.service';
declare var $;

@Component({
  selector: 'app-acesso-interno',
  templateUrl: './acesso-interno.component.html',
  styleUrls: ['./acesso-interno.component.css'],
  providers: [ AcessoService ],
  animations: [
  
    trigger('animacao-cadastro', [
      state('criado', style({
        opacity: 1
      })),
      transition('void => criado', [
        style({opacity: 0, transform:'translate(0px, -50px)'}),
        animate('500ms 0s ease-in-out')
      ])
    ]),

    trigger('animacao-fundo', [
      state('criado', style({
        opacity: 1, transform: 'skew(-22deg)'
      })),
      state('c2', style({
        opacity: 1, left: '10%', transform: 'skew(22deg)'
      })),
      transition('void => criado', [
        style({opacity: 1, transform: 'skew(0deg)'}),
        animate('500ms 1s ease-in-out')
      ]),
      transition('criado <=> c2', [
        style({opacity: 1}),
        animate('1000ms 0s ease-in-out')
      ])
    ]),

    trigger('animacao-c2', [
      state('criado', style({
        opacity: 1
      })),
      state('vazio', style({
        opacity: 0
      })),
      transition('void => criado', [
        style({opacity: 0, transform:'translate(0px, -200px)'}),
        animate('500ms 0s ease-in-out')
      ]),
      transition('vazio => criado', [
        style({opacity: 0, transform:'translate(0px, -200px)'}),
        animate('500ms 0s ease-in-out')
      ])
    ]),
    
    trigger('animacao-login', [
      state('criado', style({
        opacity: 1
      })),
      transition('void => criado', [
        style({opacity: 0, transform:'translate(0px, -50px)'}),
        animate('500ms 1s ease-in-out')
      ])
    ]),

    trigger('animacao-contador', [
      state('reload', style({
        opacity: 1
      })),
      state('load', style({
        opacity: 1
      })),
      transition('load <=> reload', [
        style({opacity: 0, transform:'translate(0px, -50px)'}),
        animate('500ms 0s ease-in-out')
      ]),
      transition('void => reload', [
        style({opacity: 0, transform:'translate(0px, -50px)'}),
        animate('500ms 0s ease-in-out')
      ])
    ]),

    trigger('animacao-load', [
      state('aberto', style({
        opacity: 0,
      })),
      transition('void => aberto', [
        style({opacity: 1, overflow: 'hidden', height: '100px', width: '100px'}),
        animate('500ms 1000ms ease-in-out',
        style({
          opacity: 1,
          overflow: 'hidden',
          height: '1500px', 
          width: '1500px'
        }))
      ])
    ]),
    trigger('animacao-fullscreen', [
      state('fechado', style({
        opacity: 1,
      })),
      state('aberto', style({
        opacity: 0,
      })),
      transition('fechado => aberto', [
        style({opacity: 1}),
        animate('500ms 0ms ease-in-out',
        style({
          opacity: 0
        }))
      ])
    ]),
    trigger('loader', [
      state('frente', style({
        transform: 'rotateY('+ 180 + 'deg)',
      })),
      state('costa', style({
        transform: 'rotateY('+ 360 + 'deg)',
      })),
      transition('frente <=> costa',
        animate('1000ms ease-in-out'))
    ])
  ]
})
export class AcessoInternoComponent implements OnInit {

  public estadoFirstLoad :string = 'aberto'
  public estadoFullscreen :string = 'fechado'
  public estadoPresents :string = 'criado'
  public estadoFundo :string = AcessoService.estadoFundo
  public estadoC2 : string = AcessoService.estadoC2
  public estadoC3 : string = AcessoService.estadoC3
  static estado : string = 'criado'
  public estadoCadastro :string = 'criado'
  public estadoLogin :string = 'criado'
  public estadoLogo: string = 'frente'

  public estadoContador : string = 'load'
  public req = 0
  public trocos : number = 150000
  public contador_trocos : Array<string> = [ ]

  public firstLoad : boolean = true

  public cadastro: boolean = false

  constructor(private auth : Auth) { }

  ngOnInit() {

    AcessoService.emitirEstadoFundo.subscribe(
      data => this.estadoFundo = data
    )

    AcessoService.emitirEstadoC2.subscribe(
      data => this.estadoC2 = data
    )

    AcessoService.emitirEstadoC3.subscribe(
      data => this.estadoC3 = data
    )

    var estado = "criado"
    var estadoC2 = "vazio"
    var estadoC3 = "vazio"

    setTimeout(()=> this.firstLoad = false,1400)

    setTimeout(()=> 
      this.estadoFullscreen='aberto',950)

      $(document).scroll(function () {
        var y = $(this).scrollTop();
        var t = $('#c2').offset().top-200;
        var t2 = $('#c3').offset().top-200;
        if (y > t && estadoC2!="criado") {
          estadoC2="criado"
          AcessoService.enviaEstadoC2("criado")
        }
        if (y > t2 && estadoC3!="criado") {
          estadoC3="criado"
          AcessoService.enviaEstadoC3("criado")
        }
    });

    $(document).scroll(function () {
      var y = $(this).scrollTop();
      var t = $('#c2').offset().top-200;
      var b = 1200;
        if (y > t && y < b) {
        if(estado ==='criado'){
          estado = 'c2'
          AcessoService.enviaEstadoFundo(estado)
        }
      } else {
        if(estado==='c2'){
          estado = 'criado'
          AcessoService.enviaEstadoFundo(estado)
        }
      }
  });
  

  $(document).ready(function(){

    //Verifica se a Janela está no topo
    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('.scrollToTop').fadeIn();
        } else {
            $('.scrollToTop').fadeOut();
        }
    });

    //Onde a mágia acontece! rs
    $('.scrollToTop').click(function(){
        $('html, body').animate({scrollTop : 0},800);
        return false;
    });

});

  }

  public exibirPainel(event: string) :void{
    this.cadastro = event === 'cadastro' ? true : false

  }

  static mudarEstadoFundo(estado){
    AcessoInternoComponent.estado = estado
  }

}
