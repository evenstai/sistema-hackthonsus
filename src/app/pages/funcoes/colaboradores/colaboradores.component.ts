import { Component, OnInit } from '@angular/core';
import { Colaboradores } from '../../../../models/colaborador.model';
import { Requests } from '../../../../services/requests.service';
import { ColaboradoresSelectedService } from 'src/services/modules_services/colaboradores.service';
import { Router } from '@angular/router';
import { EditColaboradorService } from 'src/services/modules_services/edit_colaborador.service';

@Component({
  selector: 'app-colaboradores',
  templateUrl: './colaboradores.component.html',
  styleUrls: ['./colaboradores.component.css'],
  providers: [
    ColaboradoresSelectedService,
    EditColaboradorService]
})
export class ColaboradoresComponent implements OnInit {

  estadoLoad : boolean = true
  estadoLoadConvite : boolean = false

  colaboradores : Colaboradores

  cpf_reconvite : number = null

  constructor(
    private requests : Requests,
    private rotas : Router,
    private editColaboradorService : EditColaboradorService) { }

  ngOnInit() {

    this.listarColaboradores()

    ColaboradoresSelectedService.emitirRefreshColaboradores.subscribe(
      trigger => {
        if(trigger === true){
          this.listarColaboradores()
        }
      }
    )

  }

  listarColaboradores(){
    this.mostrarLoad(true)
    this.requests.solicitarListaColaboradores()
    .subscribe(
      data => {
        this.mostrarLoad(false)
        if(data != null){
          var res_colaboradores :any = data;
          this.colaboradores = res_colaboradores.data;
        }
      }
    )
  }

  editarColaborador(colaborador){
    this.editColaboradorService.enviaInfoColaboradorConfig(colaborador)
    this.rotas.navigate(['dashboard/editar-colaborador'])
  }

  reenviarConvite(cpf_colaborador : any){
    this.estadoLoadConvite = true
    this.cpf_reconvite = cpf_colaborador
    this.requests.reenviarConvite(cpf_colaborador).subscribe(
      res => {
        this.estadoLoadConvite = false
        this.cpf_reconvite = null
      }
    )
  }

  mostrarLoad(status :boolean){
    this.estadoLoad = status
  }

}


