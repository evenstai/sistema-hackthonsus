import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ImagemModuleService } from 'src/services/modules_services/enviar_imagem.service';
import { ResponsesModuleService } from 'src/services/modules_services/responses_module.service';
import { Requests } from 'src/services/requests.service';
declare var $;

@Component({
  selector: 'app-produtos',
  templateUrl: './produtos.component.html',
  styleUrls: ['./produtos.component.css'],
  providers: [
    ImagemModuleService,
    ResponsesModuleService]
})
export class ProdutosComponent implements OnInit {

  public formProduto :FormGroup = new FormGroup({
    'nome' : new FormControl(null),
    'descricao' : new FormControl(null)
  })

  public formAddMarca :FormGroup = new FormGroup({
    'marca' : new FormControl(null)
  })

  public formAddCategoria :FormGroup = new FormGroup({
    'categoria' : new FormControl(null)
  })

  public responseScreen : boolean = false
  public responseScreenAddMarca : boolean = false
  public responseScreenAddCategoria : boolean = false

  public estadoLoad : boolean = false
  public estadoLoadAddMarca : boolean = false
  public estadoLoadAddCategoria : boolean = false

  public mensagem_loader : string = ""

  public img_produto : string = "/assets/new_product_img.svg"
  public img_base64 : string = null

  public id_categoria : number = null
  public id_marca : number = null

  public categorias : any
  public marcas : any

  public erro_categoria : String = null
  public erro_marca : String = null

  public estadoForm : string = "disabled"
  public estadoFormMarca : string = "disabled"
  public estadoFormCategoria : string = "disabled"

  public nomeValido : boolean = false
  public descricaoValida : boolean = false
  public marcaValida : boolean = false
  public categoriaValida : boolean = false

  constructor(
    private responsesModuleService : ResponsesModuleService,
    private requests : Requests) { }

  ngOnInit() {
    this.listarCategorias()
    this.listarMarcas()

    ImagemModuleService.emitirImagemBase64.subscribe(
      imagem => {
        this.img_produto = imagem
        this.img_base64 = imagem
      }
    )

    ResponsesModuleService.emitirFecharResponseModule.subscribe(
      trigger => {
        if(trigger){
          $('#exampleModall').modal('hide');
        }else{
          if(this.responseScreen){
            this.mostrarResponseScreen(false, null, null, null)
          }
          if(this.responseScreenAddMarca){
            this.mostrarResponseScreenAddMarca(false, null, null, null)
          }
          if(this.responseScreenAddCategoria){
            this.mostrarResponseScreenAddCategoria(false, null, null, null)
          }
        }
      }
    )
  }

  cadastrarProduto(){
    this.mostrarLoader(true, "Cadastrando Produto")
    var dados_produto : any = {
      'img_base64' : this.img_base64,
      'nome' : this.formProduto.value.nome,
      'descricao' : this.formProduto.value.descricao,
      'id_categoria' : this.id_categoria
    }

    this.requests.cadastrarProduto(dados_produto).subscribe(
      res => {
        this.mostrarLoader(false, null)
        var resp_cadastro : any = res
        console.log(resp_cadastro)
        if(resp_cadastro.code===200){
          this.novoCadastro()
        }else{

        }
        this.mostrarResponseScreen(
          true,
          resp_cadastro.code,
          resp_cadastro.mensagem,
          "400px"
        )
      }
    )
  }

  cadastrarCategoria(){
    this.mostrarLoaderAddCategoria(true, "Adicionando Categoria")
    let dados_categoria : any = {
      'categoria' : this.formAddCategoria.value.categoria
    }

    this.requests.cadastrarCategoria(dados_categoria).subscribe(
      res => {
        this.mostrarLoaderAddCategoria(false, null)

        var resp_categoria : any = res
        if(resp_categoria.code === 200){
          this.categorias = resp_categoria.data
          this.erro_categoria = null
          this.formAddCategoria.reset()
          this.categoriaValida = false
        }
        this.mostrarResponseScreenAddCategoria(
          true,
          resp_categoria.code,
          resp_categoria.mensagem,
          "300px"
        )
      }
    )
  }

  cadastrarMarca(){
    this.mostrarLoaderAddMarca(true, "Adicionando Marca");
    let dados_marca : any = {
      'marca' : this.formAddMarca.value.marca
    }

    this.requests.cadastrarMarca(dados_marca).subscribe(
      res => {
        this.mostrarLoaderAddMarca(false, null);
        var resp_marca : any = res
        if(resp_marca.code === 200){
          this.marcas = resp_marca.data
          this.erro_marca = null
          this.formAddMarca.reset()
          this.marcaValida = false
        }
        this.mostrarResponseScreenAddMarca(
          true,
          resp_marca.code,
          resp_marca.mensagem,
          "300px"
        )
      }
    )
  }

  listarMarcas(){
    this.requests.listarMarcas().subscribe(
      res => {
        var resp_marcas : any = res
        if(resp_marcas.code==200){
          this.marcas = resp_marcas.data
          console.log("MARCAS :", this.marcas)
        }else{
          this.erro_marca = resp_marcas.mensagem
        }
        console.log(resp_marcas)
      }
    )
  }

  listarCategorias(){
    this.requests.listarCategorias().subscribe(
      res => {
        var resp_categorias : any = res
        if(resp_categorias.code==200){
          this.categorias = resp_categorias.data
          console.log("CATEGORIAS :", this.categorias)
        }else{
          this.erro_categoria = resp_categorias.mensagem
        }
        console.log(resp_categorias)
      }
    )
  }

  validaNome(nome : string){
    if(this.formProduto.value.nome.length>5){
      this.nomeValido = true  
    }else{
      this.nomeValido = false
    }
    this.ativarBotao()
  }

  validaDescricao(){
    if(this.formProduto.value.descricao.length<500 && this.formProduto.value.descricao.length>2){
      this.descricaoValida = true
    }else{
      this.descricaoValida = false
    }
    this.ativarBotao()
  }

  validaAddCategoria(){
    if(this.formAddCategoria.value.categoria.length<500 && this.formAddCategoria.value.categoria.length>2){
      this.categoriaValida = true
    }else{
      this.categoriaValida = false
    }
    this.ativarBotaoAddCategoria()
  }

  validaAddMarca(){
    if(this.formAddMarca.value.marca.length<150 && this.formAddMarca.value.marca.length>4){
      this.marcaValida = true
    }else{
      this.marcaValida = false
    }
    this.ativarBotaoAddMarca()
  }

  ativarBotaoAddMarca(){
    if(this.marcaValida != false){
      this.estadoFormMarca = ''
    }else{
      this.estadoFormMarca = 'disabled'
    }
  }

  ativarBotaoAddCategoria(){
    if(this.categoriaValida != false){
      this.estadoFormCategoria = ''
    }else{
      this.estadoFormCategoria = 'disabled'
    }
  }

  ativarBotao(){
    if(this.nomeValido != false){
      this.estadoForm = ''
    }else{
      this.estadoForm = 'disabled'
    }
  }

  novoCadastro(){
    this.formProduto.reset()
    this.img_produto = "/assets/new_user_img.svg"
  }

  mostrarResponseScreen(status: boolean, code: number, mensagem: string, altura: string){
    this.responseScreen = status
    this.responsesModuleService.enviaConfigResponse(code, mensagem, altura)
  }

  mostrarResponseScreenAddMarca(status: boolean, code: number, mensagem: string, altura: string){
    this.responseScreenAddMarca = status
    this.responsesModuleService.enviaConfigResponse(code, mensagem, altura)
  }

  mostrarResponseScreenAddCategoria(status: boolean, code: number, mensagem: string, altura: string){
    this.responseScreenAddCategoria = status
    this.responsesModuleService.enviaConfigResponse(code, mensagem, altura)
  }

  mostrarLoader(status: boolean, mensagem: string){
    this.estadoLoad = status
    this.mensagem_loader = mensagem
  }

  mostrarLoaderAddMarca(status: boolean, mensagem: string){
    this.estadoLoadAddMarca = status
    this.mensagem_loader = mensagem
  }

  mostrarLoaderAddCategoria(status: boolean, mensagem: string){
    this.estadoLoadAddCategoria = status
    this.mensagem_loader = mensagem
  }


}
