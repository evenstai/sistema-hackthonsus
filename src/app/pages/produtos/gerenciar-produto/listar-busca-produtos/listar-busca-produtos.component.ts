import { Component, OnInit } from '@angular/core';
import { ListaProdutoService } from 'src/services/modules_services/lista_produtos.service';
import { Constants } from 'src/utils/Constantes';
import { EditProdutoService } from 'src/services/modules_services/edit_produto.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listar-busca-produtos',
  templateUrl: './listar-busca-produtos.component.html',
  styleUrls: ['./listar-busca-produtos.component.css'],
  providers: [ 
    ListaProdutoService,
    EditProdutoService ]
})
export class ListarBuscaProdutosComponent implements OnInit {

  public dados_produtos : any = ListaProdutoService.produtos

  public url_imagens : string = Constants.API_IMAGENS_PRODUTOS

  constructor(
    private rotas : Router,
    private editProdutoService : EditProdutoService) { }

  ngOnInit() {
  }

  editarProduto(produto){
    this.editProdutoService.enviaEditProduto(produto)
    this.rotas.navigate(['dashboard/editar-produto'])
  }

  mostrarImg(img : String){
    if(img!=null){
      return this.url_imagens+img
    }else{
      return "/assets/new_product_img.svg"
    }
  }

  showDescricao(descricao){
    if(descricao!=null){
      return descricao.substr(0,90) + " ..."
    }else{
      return "Produto sem descrição."
    }
  }

}
