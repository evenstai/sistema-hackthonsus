import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Requests } from 'src/services/requests.service';
import { ListaProdutoService } from 'src/services/modules_services/lista_produtos.service';

@Component({
  selector: 'app-gerenciar-produto',
  templateUrl: './gerenciar-produto.component.html',
  styleUrls: ['./gerenciar-produto.component.css'],
  providers: [ ListaProdutoService ]
})
export class GerenciarProdutoComponent implements OnInit {

  public formBuscarProduto : FormGroup = new FormGroup({
    'nome' : new FormControl(null)
  });

  public id_categoria : number = 0
  public id_marca : number = 0

  public categorias : any
  public marcas : any

  public estadoLoad : boolean = false
  public mensagem_loader : string

  public produtos_validos : boolean = true
  public msg_produtos_erro : string

  constructor(
    private requests : Requests, 
    private listaProdutoService : ListaProdutoService ) { }

  ngOnInit() {
    this.listarProdutos()
    this.listarCategorias()
    this.listarMarcas()
  }

  listarProdutos(){
    this.mostrarLoader(true, "Buscando Produtos");

    let dados_produtos : any = {
      'limit' : 20,
      'offset' : 0
    }

    this.requests.listarProdutos(dados_produtos).subscribe(
      res => {
        this.mostrarLoader(false, null)
        var resp_lista: any = res
        if(resp_lista.code==200){
          this.mostrarProdutos(true, null)
          this.listaProdutoService.enviaProduto(resp_lista.data);
        }else{
          this.mostrarProdutos(false, resp_lista.mensagem)
        }
      }
    )

  }

  buscarProdutoViaInput(){
    if(this.formBuscarProduto.value.nome.length>=3){
      this.buscarProduto()
    }
  }

  buscarProduto(){

      this.mostrarLoader(true, "Buscando Produtos");

      let dados_produtos : any = {
        'nome' : this.formBuscarProduto.value.nome,
        'categoria' : this.id_categoria,
        'marca' : this.id_marca,
        'limit' : 20,
        'offset' : 0
      }
  
      this.requests.buscarProdutos(dados_produtos).subscribe(
        res => {
          this.mostrarLoader(false, null)
          var resp_busca : any = res
          if(resp_busca.code==200){
            this.mostrarProdutos(true, null)
            this.listaProdutoService.enviaProduto(resp_busca.data);
          }else{
            this.mostrarProdutos(false, resp_busca.mensagem)
          }
        }
      )
  }

  listarMarcas(){
    this.requests.listarMarcas().subscribe(
      res => {
        var resp_marcas : any = res
        if(resp_marcas.code==200){
          this.marcas = resp_marcas.data
          console.log("MARCAS :", this.marcas)
        }else{
          //this.erro_marca = resp_marcas.mensagem
        }
        console.log(resp_marcas)
      }
    )
  }

  listarCategorias(){
    this.requests.listarCategorias().subscribe(
      res => {
        var resp_categorias : any = res
        if(resp_categorias.code==200){
          this.categorias = resp_categorias.data
          console.log("CATEGORIAS :", this.categorias)
        }else{
          //this.erro_categoria = resp_categorias.mensagem
        }
        console.log(resp_categorias)
      }
    )
  }

  mostrarLoader(status: boolean, mensagem : string){
    this.estadoLoad = status
    this.mensagem_loader = mensagem
  }

  mostrarProdutos(status: boolean, mensagem_erro : string){
    this.produtos_validos = status
    this.msg_produtos_erro = mensagem_erro
  }

}
