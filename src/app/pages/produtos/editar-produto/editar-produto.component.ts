import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Constants } from 'src/utils/Constantes';
import { Router } from '@angular/router';
import { Requests } from 'src/services/requests.service';
import { EditProdutoService } from 'src/services/modules_services/edit_produto.service';
import { ResponsesModuleService } from 'src/services/modules_services/responses_module.service';
import { ImagemModuleService } from 'src/services/modules_services/enviar_imagem.service';
declare var $;

@Component({
  selector: 'app-editar-produto',
  templateUrl: './editar-produto.component.html',
  styleUrls: ['./editar-produto.component.css'],
  providers: [ 
    EditProdutoService,
    ResponsesModuleService,
    ImagemModuleService ]
})
export class EditarProdutoComponent implements OnInit {
  public formEditarProduto :FormGroup = new FormGroup({
    'nome' : new FormControl(null),
    'descricao' : new FormControl(null),
    'mensagem' : new FormControl(null),
    'categoria' : new FormControl(null),
  })

  public estadoLoad : boolean = false
  public responseScreen : boolean = false
  public mensagem_loader : string = ""

  public estadoForm : string = "disabled"

  public produto : any = EditProdutoService.produto
  public id_produto : number

  public id_categoria : number
  public id_marca : number

  public categorias : any
  public marcas : any

  public text_button_img : string = "ADICIONAR FOTO"

  public img_produto : string = "/assets/new_product_img.svg"
  public url_img_produto : string = Constants.API_IMAGENS_PRODUTOS
  public img_base64 : string = null

  public nomeValido : boolean = false
  public descricaoValida : boolean = false

  constructor(
    private rotas : Router,
    private requests : Requests,
    private responsesModuleService : ResponsesModuleService ) { }

  ngOnInit() {
    this.setInfo();
    this.listarCategorias();
    this.listarMarcas();

    ImagemModuleService.emitirImagemBase64.subscribe(
      imagem => {
        this.img_produto = imagem
        this.img_base64 = imagem
      }
    )

    ResponsesModuleService.emitirFecharResponseModule.subscribe(
      trigger => {
        if(trigger){
          $('#exampleModall').modal('hide');
        }else{
          if(this.responseScreen){
            this.mostrarResponseScreen(false, null, null, null)
          }
        }
      }
    )
  }

  setInfo() {
    if (this.produto != undefined) {
      this.formEditarProduto.get('nome').setValue(this.produto.nome)
      this.formEditarProduto.get('descricao').setValue(this.produto.descricao)
      this.id_categoria = this.produto.id_categoria
      this.id_marca = this.produto.id_marca

      this.id_produto = this.produto.id

      this.validaNome("")
      this.validaDescricao()

      if(this.produto.img!=null){
        this.img_produto = this.url_img_produto+this.produto.img
        this.text_button_img = "ALTERAR FOTO"
      }

    } else {
      this.rotas.navigate(["/dashboard/gerenciar-produto"])
    }
  }

  atualizarDadosProduto(){
    let dados_produto : any = {
      'nome' : this.formEditarProduto.value.nome,
      'descricao' : this.formEditarProduto.value.descricao,
      'categoria' : this.id_categoria,
      'marca' : null,
      'img_base64' : this.img_base64
    }

    //this.requests.atualizarProduto(dados_produto).subscribe
  }

  validaNome(nome : string){
    if(this.formEditarProduto.value.nome.length>3){
      this.nomeValido = true  
    }else{
      this.nomeValido = false
    }
    this.ativarBotao()
  }

  validaDescricao(){
    if(this.formEditarProduto.value.descricao.length<500 && this.formEditarProduto.value.descricao.length>2){
      this.descricaoValida = true
    }else{
      this.descricaoValida = false
    }
    this.ativarBotao()
  }

  ativarBotao(){
    if(this.nomeValido != false && this.descricaoValida != false && this.id_categoria != 0 && this.id_marca != 0){
      this.estadoForm = ''
    }else{
      this.estadoForm = 'disabled'
    }
  }

  listarMarcas(){
    this.requests.listarMarcas().subscribe(
      res => {
        var resp_marcas : any = res
        if(resp_marcas.code==200){
          this.marcas = resp_marcas.data
          console.log("MARCAS :", this.marcas)
        }else{
          //this.erro_marca = resp_marcas.mensagem
        }
        console.log(resp_marcas)
      }
    )
  }

  listarCategorias(){
    this.requests.listarCategorias().subscribe(
      res => {
        var resp_categorias : any = res
        if(resp_categorias.code==200){
          this.categorias = resp_categorias.data
          console.log("CATEGORIAS :", this.categorias)
        }else{
          //this.erro_categoria = resp_categorias.mensagem
        }
        console.log(resp_categorias)
      }
    )
  }

  mostrarResponseScreen(status: boolean, code: number, mensagem: string, altura: string){
    this.responseScreen = status
    this.responsesModuleService.enviaConfigResponse(code, mensagem, altura)
  }

  mostrarLoader(status: boolean, mensagem: string){
    this.estadoLoad = status
    this.mensagem_loader = mensagem
  }


}
