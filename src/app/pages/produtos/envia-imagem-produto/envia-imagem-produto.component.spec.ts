import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnviaImagemProdutoComponent } from './envia-imagem-produto.component';

describe('EnviaImagemProdutoComponent', () => {
  let component: EnviaImagemProdutoComponent;
  let fixture: ComponentFixture<EnviaImagemProdutoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnviaImagemProdutoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnviaImagemProdutoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
