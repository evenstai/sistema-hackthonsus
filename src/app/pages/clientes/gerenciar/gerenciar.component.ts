import { Component, OnInit } from '@angular/core';
import { Requests } from 'src/services/requests.service';
import { ListaBuscaClienteService } from 'src/services/modules_services/lista_busca_cliente.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-gerenciar',
  templateUrl: './gerenciar.component.html',
  styleUrls: ['./gerenciar.component.css'],
  providers: [ ListaBuscaClienteService ]
})
export class GerenciarComponent implements OnInit {

  public formBuscaCliente :FormGroup = new FormGroup({
    'nome' : new FormControl(null)
  })

  public estadoLoad : boolean = false
  public clientes_validos : boolean

  public mensagem_loader : string
  public msg_clientes_erro : string

  constructor(
    private requests : Requests,
    private listaBuscaClienteService : ListaBuscaClienteService) { }

  ngOnInit() {
    this.listarClientes()
  }

  listarClientes(){
    this.mostrarLoader(true, "Listando Clientes")
    let dados_clientes = {
      'limit' : 20,
      'offset' : 0
    }

    this.requests.listarCliente(dados_clientes).subscribe(
      res => {
        this.mostrarLoader(false, null)
        var resp_clientes : any = res
        if(resp_clientes.code===200){
          this.listaBuscaClienteService.enviaCliente(resp_clientes.data);
          this.mostrarClientes(true, "")
        }else{
          this.mostrarClientes(false, resp_clientes.mensagem)
        }
      }
    )

  }

  buscarClienteViaInput(){
    if(this.formBuscaCliente.value.nome.length>=3){
      this.buscarCliente()
    }
  }

  buscarCliente(){
    this.mostrarLoader(true, "Listando Clientes")
    let dados_clientes = {
      'nome' : this.formBuscaCliente.value.nome,
      'limit' : 20,
      'offset' : 0
    }

    this.requests.buscarClienteNome(dados_clientes).subscribe(
      res => {
        this.mostrarLoader(false, null)
        var resp_clientes : any = res
        if(resp_clientes.code===200){
          this.listaBuscaClienteService.enviaCliente(resp_clientes.data);
          this.mostrarClientes(true, "")
        }else{
          this.mostrarClientes(false, resp_clientes.mensagem)
        }
      }
    )
  }

  mostrarLoader(status: boolean, mensagem : string){
    this.estadoLoad = status
    this.mensagem_loader = mensagem
  }

  mostrarClientes(status: boolean, mensagem_erro : string){
    this.clientes_validos = status
    this.msg_clientes_erro = mensagem_erro
  }

}
