import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NutrirEstoqueComponent } from './nutrir-estoque.component';

describe('NutrirEstoqueComponent', () => {
  let component: NutrirEstoqueComponent;
  let fixture: ComponentFixture<NutrirEstoqueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NutrirEstoqueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NutrirEstoqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
