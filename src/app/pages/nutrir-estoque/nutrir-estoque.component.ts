import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ValidaCpf } from 'src/utils/valida-cpf';
import { Requests } from 'src/services/requests.service';
import { TelefoneValidation } from 'src/utils/valida-telefone';
import { ImagemModuleService } from 'src/services/modules_services/enviar_imagem.service';
import { ResponsesModuleService } from 'src/services/modules_services/responses_module.service';
import { TogglePasswordService } from 'src/services/modules_services/toggle_password.service';
declare var $;
@Component({
  selector: 'app-nutrir-estoque',
  templateUrl: './nutrir-estoque.component.html',
  styleUrls: ['./nutrir-estoque.component.css'],
  providers: [
    ValidaCpf,
    TelefoneValidation,
    ImagemModuleService,
    ResponsesModuleService,
    TogglePasswordService]
})
export class NutrirEstoqueComponent implements OnInit {
  public responseScreen : boolean = false;
  public estadoLoad : boolean = false
  public erro_medicamento : String = null
  public medicamentos : any

  id_medicamento

  public mensagem_loader : string = ""

  public celular_mask = ['(', /\d/, /\d/,')', /\d/, /\d/,/\d/,/\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public cpf_mask = [/\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '-', /\d/, /\d/]
  public birth_mask = [/\d/, /\d/,'/', /\d/, /\d/, '/', /\d/,/\d/,/\d/,/\d/]

  public formNota :FormGroup = new FormGroup({
    'nota' : new FormControl(null),
    'serie' : new FormControl(null),
    'fornecedor' : new FormControl(null)
  })

  public formMedicacao :FormGroup = new FormGroup({
    'medicacao' : new FormControl(null),
    'quantidade' : new FormControl(null),
    'dosagem' : new FormControl(null),
    'preco' : new FormControl(null)
  })


  public notaValida : boolean = false
  public serieValida : boolean = false
  public fornecedorValido : boolean = false
  public medicacaoValida : boolean = false

  public estadoForm : string = "disabled"

  constructor(private validaCpf : ValidaCpf,
    private requests : Requests,
    private responsesModuleService : ResponsesModuleService) { }

  ngOnInit() {

    ResponsesModuleService.emitirFecharResponseModule.subscribe(
      trigger => {
        if(trigger){
          $('#exampleModall').modal('hide');
        }else{
          this.mostrarResponseScreen(false, null, null, null)
          //this.inputsCadastroUnidade = (this.keepCadastro) ? true : false
        }
      }
    )

    this.listarMedicamentos()
  }
  validaNota(nota : string){
    if(this.formNota.value.nota.length<7 || this.formNota.value.nota.indexOf(" ") == -1){
      this.notaValida = false
    }else{
      this.notaValida = true
    }
    this.ativarBotao()
  }
  validaSerie(serie : string){
    if(this.formNota.value.serie.length<7 || this.formNota.value.serie.indexOf(" ") == -1){
      this.serieValida = false
    }else{
      this.serieValida = true
    }
    this.ativarBotao()
  }

  validaMedicamento(medicamento : string){
    if(this.formMedicacao.value.medicamento.length<7 || this.formMedicacao.value.medicamento.indexOf(" ") == -1){
      this.medicacaoValida = false;
    }else{
      this.medicacaoValida = true;
    }
    this.ativarBotao();
  }
  validaFornecedor(fornecedor : string){
    if(this.formNota.value.fornecedor.length<7 || this.formNota.value.fornecedor.indexOf(" ") == -1){
      this.fornecedorValido = false;
    }else{
      this.fornecedorValido = true;
    }
    this.ativarBotao();
  }

  ativarBotao(){
    if(this.validaNota && this.validaSerie && this.fornecedorValido){
      this.estadoForm = ''
    }else{
      this.estadoForm = 'disabled'
    }
  }

  cadastrarMedicacao(){
    this.mostrarLoader(true, "Cadastrando Nota Fiscal")
    var dados_nota : any = {
      'numeroNota' : this.formNota.value.nota,
      'serie' : this.formNota.value.serie,
      'id_fornecedor' : this.formNota.value.fornecedor,
      'id_produto' : this.id_medicamento,
      'quantidade' : this.formMedicacao.value.quantidade,
      'dosagem' : this.formMedicacao.value.dosagem,
      'preco' : this.formMedicacao.value.preco,
      'lote' : this.formMedicacao.value.lote,
      'total' : this.formMedicacao.value.quantidade,
      'saldo' : this.formMedicacao.value.quantidade
    }

    this.requests.insertNota(dados_nota).subscribe(
      res => {
        this.mostrarLoader(false, null)
        var resp_nota : any = res
        if(resp_nota.code===200){
          this.novoCadastro()
        }else{

        }
        this.mostrarResponseScreen(
          true,
          resp_nota.code,
          resp_nota.mensagem,
          "400px"
        )
      }
    )
  }

  listarMedicamentos(){
    let dados_produtos: any = {limit:20, offset:0}
    this.requests.listarProdutos(dados_produtos).subscribe(
      res => {
        var resp_medicamentos : any = res
        if(resp_medicamentos.code==200){
          this.medicamentos = resp_medicamentos.data

        }else{
          this.erro_medicamento = resp_medicamentos.mensagem
        }
        console.log(resp_medicamentos)
      }
    )
  }

  novoCadastro(){
    this.formMedicacao.reset()
  }

  mostrarResponseScreen(status: boolean, code: number, mensagem: string, altura: string){
    this.responseScreen = status
    this.responsesModuleService.enviaConfigResponse(code, mensagem, altura)
  }

  mostrarLoader(status: boolean, mensagem: string){
    this.estadoLoad = status
    this.mensagem_loader = mensagem
  }

}

