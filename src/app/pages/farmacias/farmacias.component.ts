import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ValidaCpf } from 'src/utils/valida-cpf';
import { Requests } from 'src/services/requests.service';
import { TelefoneValidation } from 'src/utils/valida-telefone';
import { ImagemModuleService } from 'src/services/modules_services/enviar_imagem.service';
import { ResponsesModuleService } from 'src/services/modules_services/responses_module.service';
import { TogglePasswordService } from 'src/services/modules_services/toggle_password.service';
declare var $;

@Component({
  selector: 'app-farmacias',
  templateUrl: './farmacias.component.html',
  styleUrls: ['./farmacias.component.css'],
  providers: [ 
    ValidaCpf,
    TelefoneValidation,
    ImagemModuleService,
    ResponsesModuleService,
    TogglePasswordService]
})
export class FarmaciasComponent implements OnInit {

  public responseScreen : boolean = false
  public estadoLoad : boolean = false

  public marcas : any
  public erro_marca : String = null

  public mensagem_loader : string = ""

  public celular_mask = ['(', /\d/, /\d/,')', /\d/, /\d/,/\d/,/\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public cpf_mask = [/\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '-', /\d/, /\d/]
  public birth_mask = [/\d/, /\d/,'/', /\d/, /\d/, '/', /\d/,/\d/,/\d/,/\d/]

  public formFarmacia :FormGroup = new FormGroup({
    'nome_unidade' : new FormControl(null),
    'codigo_unidade' : new FormControl(null),
    'endereco' : new FormControl(null),
    'bairro' : new FormControl(null),
    'horario_abre' : new FormControl(null),
    'horario_fecha' : new FormControl(null)
  })

  public latitude_unidade = 0
  public longitude_unidade = 0

  public comissionado : boolean = false
  public status_comissionado : number = 0

  public show_password : boolean = false
  public togglePassword : boolean = false

  public img_profissional : string = "/assets/new_user_img.svg"
  public img_base64 : string = null

  public nomeValido : boolean = false
  public cpfValido : boolean = false
  public enderecoValido : boolean = false
  public codValido : boolean = false
  public emailValido : boolean = false
  public telefoneValido : boolean = false
  public data_nascValido : boolean = false
  public senhaValido : boolean = false

  public estadoForm : string = "disabled"

  constructor(private validaCpf : ValidaCpf,
    private requests : Requests,
    private validaTelef : TelefoneValidation,
    private responsesModuleService : ResponsesModuleService,
    private togglePasswordService : TogglePasswordService) { }

  ngOnInit() {
    ImagemModuleService.emitirImagemBase64.subscribe(
      imagem => {
        this.img_profissional = imagem
        this.img_base64 = imagem
      }
    )

    ResponsesModuleService.emitirFecharResponseModule.subscribe(
      trigger => {
        if(trigger){
          $('#exampleModall').modal('hide');
        }else{
          this.mostrarResponseScreen(false, null, null, null)
          //this.inputsCadastroUnidade = (this.keepCadastro) ? true : false
        }
      }
    )

    this.listarMarcas()
  }

  listarMarcas(){
    this.requests.listarMarcas().subscribe(
      res => {
        var resp_marcas : any = res
        if(resp_marcas.code==200){
          this.marcas = resp_marcas.data
          console.log("MARCAS :", this.marcas)
        }else{
          this.erro_marca = resp_marcas.mensagem
        }
        console.log(resp_marcas)
      }
    )
  }

  validaNome(nome : string){
    if(this.formFarmacia.value.nome.length<7 || this.formFarmacia.value.nome.indexOf(" ") == -1){
      this.nomeValido = false  
    }else{
      this.nomeValido = true
    }
    this.ativarBotao()
  }

  validaCPF(cpf){
    this.cpfValido = this.validaCpf.validarCPF(this.formFarmacia.value.cpf)
    this.ativarBotao()
  }


  validaEndereco(endereco : string){
    if(this.formFarmacia.value.endereco.length>5){
      this.enderecoValido = true
      
    }else{
      this.enderecoValido = false
    }
    this.ativarBotao()
  }

  validaCodigo(cod : string){
    if(this.formFarmacia.value.codigo_profissional.length>3){
      this.codValido = true
      
    }else{
      this.codValido = false
    }
    this.ativarBotao()
  }

  validaEmail(email_teste: string){
    if(this.formFarmacia.value.email.indexOf('@') == -1 || this.formFarmacia.value.email.indexOf('.') == -1 ){
      this.emailValido = false
    }else{
      this.emailValido = true
    }
    this.ativarBotao()
  }

  validaTelefone(telefone){
    this.telefoneValido = this.validaTelef.validarTel(this.formFarmacia.value.telefone)
    this.ativarBotao()
  }

  validaData(data){
    var dt = this.formFarmacia.value.data_nasc.replace("/","").replace("/","")
    var dataFinal = dt.replace(/_/g,"")
    if(dataFinal.toString().length == 8){
      var dia = dataFinal.substring(0,2)
      var mes = dataFinal.substring(2,4)
      var ano = dataFinal.substring(4)
      var ano_atual = new Date
      var ano_atual_final = ano_atual.getFullYear()-18
      if(dia<=31 && mes<=12 && ano<=ano_atual_final && ano>1900){
        this.data_nascValido = true
      }else{
        this.data_nascValido = false
      }
    }else{
      this.data_nascValido = false
    }
    this.ativarBotao()
  }

  validaSenha(senha: string){
    if(this.formFarmacia.value.senha.length>7){
      this.senhaValido = true
    }else{
      this.senhaValido = false
    }
    this.mostrarTogglePassword()
    this.ativarBotao()
  }

  ativarBotao(){
    if(this.nomeValido && this.cpfValido && this.telefoneValido && this.emailValido && 
      this.codValido && this.enderecoValido && this.data_nascValido && this.senhaValido){
      this.estadoForm = ''
    }else{
      this.estadoForm = 'disabled'
    }
  }

  cadastrarFarmacia(){
    this.mostrarLoader(true, "Cadastrando Farmacia")
    var dados_farmacia : any = {
      'nome_unidade' : this.formFarmacia.value.nome_unidade,
      'unidade' : this.formFarmacia.value.codigo_unidade,
      'endereco' : this.formFarmacia.value.endereco,
      'bairro' : this.formFarmacia.value.bairro,
      'latitude' : this.latitude_unidade,
      'longitude' : this.longitude_unidade,
      'horario_abre' : this.formFarmacia.value.horario_abre,
      'horario_fecha' : this.formFarmacia.value.horario_fecha
    }

    this.requests.insertFarmacia(dados_farmacia).subscribe(
      res => {
        this.mostrarLoader(false, null)
        var resp_cadastro : any = res
        console.log(resp_cadastro)
        if(resp_cadastro.code===200){
          this.novoCadastro()
        }else{
          
        }
        this.mostrarResponseScreen(
          true,
          resp_cadastro.code,
          resp_cadastro.mensagem,
          "400px"
        )
      }
    )
  }

  novoCadastro(){
    this.formFarmacia.reset()
    this.img_profissional = "/assets/new_user_img.svg"
  }

  putProfissionalComissionado(){
    this.comissionado = !this.comissionado
    this.comissionado?this.status_comissionado = 1: this.status_comissionado = 0
  }

  mostrarResponseScreen(status: boolean, code: number, mensagem: string, altura: string){
    this.responseScreen = status
    this.responsesModuleService.enviaConfigResponse(code, mensagem, altura)
  }

  mostrarLoader(status: boolean, mensagem: string){
    this.estadoLoad = status
    this.mensagem_loader = mensagem
  }

  mostrarPassword(){
    this.show_password = !this.show_password
    if(this.show_password){
      this.togglePasswordService.enviaMostrarPassword(true)
    }else{
      this.togglePasswordService.enviaMostrarPassword(false)
    }
  }

  mostrarTogglePassword(){
    if(this.formFarmacia.value.senha.length>0){
      this.togglePassword = true
    }else{
      this.togglePassword = false
    }
  }

}

