import { Component, OnInit, EventEmitter } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Auth } from '../../../services/auth.service';
import { Router } from '@angular/router';
import { TopoMenuModuleService } from 'src/services/modules_services/topo_menu.module.service';
import { Requests } from 'src/services/requests.service';
import { Constants } from 'src/utils/Constantes';


@Component({
  selector: 'app-topo',
  templateUrl: './topo.component.html',
  styleUrls: ['./topo.component.css'],
  providers: [ TopoMenuModuleService ],
  animations:[
    trigger('nomemenu', [
      state('hide', style({
        opacity: 0
      })),
      state('show', style({
        opacity: 1
      })),
      transition('hide => show', animate('1s ease-in')),
      transition('show => hide', animate('0.2s ease-in'))
    ])
  ]
})
export class TopoComponent implements OnInit {

  estadoLoad : boolean
  resposta_logout :any = []

  colaborador :any
  unidade : any
  nome_colaborador :string

  url_img_prof : string = Constants.API_IMAGENS_PROFISSIONAIS
  url_img_colab : string = Constants.API_IMAGENS

  cpf_colaborador :number
  funcionalidade :any
  is_admin :number
  tipo_usuario : number

  funcionalidades :any

  img_user : string

  public imagem_usuario : string = "/assets/new_user_img.svg"

  constructor(private auth: Auth, private rotas: Router, 
    private topoMenuModuleService : TopoMenuModuleService,
    private requests : Requests ) { }

  

  ngOnInit() {
    
    this.dadosDaConta()
    this.funcionalidadesDoColaborador()

    //this.imagem_unidade = this.requests.apiUrlImg+this.img_unidade
    //this.imagem_usuario = this.img_user
  }

  ativo: boolean = false
  public estado: string = 'show'
  public estado1: string = 'hide'

  setStyles(){
    this.ativo = !this.ativo;
    this.estado = this.estado === 'show' ? 'hide' : 'show'
    this.estado1 = this.estado1 === 'hide' ? 'show' : 'hide'
    this.topoMenuModuleService.enviaAtivacaoMenu(this.ativo, this.estado, this.estado1)

  }

  dadosDaConta(){

    var dados_conta = JSON.parse(localStorage.getItem('dados_conta'))
    this.colaborador = dados_conta.usuario
    this.funcionalidade = dados_conta.funcionalidades

    this.cpf_colaborador = this.colaborador[0].cpf
    this.nome_colaborador = this.colaborador[0].nome
    this.tipo_usuario = this.funcionalidade[0].tipo_usuario
    this.is_admin = this.colaborador.admin
    this.imagem_usuario = this.colaborador[0].img
    if(this.colaborador[0].img!=null){
      this.img_user = this.colaborador[0].img
    }

  }

  funcionalidadesDoColaborador(){
    var dados_conta = JSON.parse(localStorage.getItem('dados_conta'))
    this.funcionalidades = dados_conta.funcionalidades
  }

  loader(status:boolean){
    this.estadoLoad = status
  }

  mostrarImg(img : String){
    if(img!=null){
      if(this.tipo_usuario===2){
        return this.url_img_prof+img
      }else{
        return img
      }
    }else{
      return "/assets/new_user_img.svg"
    }
  }

}
