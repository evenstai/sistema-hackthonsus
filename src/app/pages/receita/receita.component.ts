import { Component, OnInit } from '@angular/core';
import { ReceitaService } from 'src/services/modules_services/receita.service';

@Component({
  selector: 'app-receita',
  templateUrl: './receita.component.html',
  styleUrls: ['./receita.component.css'],
  providers: [ ReceitaService ]
})
export class ReceitaComponent implements OnInit {

  public dados_receita : any = ReceitaService.dados_receita

  constructor() { }

  ngOnInit() {
    
  }

}
