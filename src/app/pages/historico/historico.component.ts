import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { IMyDpOptions } from 'mydatepicker';
import { Requests } from 'src/services/requests.service';
import { ValidaCpf } from 'src/utils/valida-cpf';

@Component({
  selector: 'app-historico',
  templateUrl: './historico.component.html',
  styleUrls: ['./historico.component.css'],
  providers: [
    ValidaCpf ]
})
export class HistoricoComponent implements OnInit {
  public cpf_mask = [/\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '-', /\d/, /\d/]

  public mensagem_loader : string

  public id_profissional_escolhido : number
  public inicio
  public fim

  public lista_historicos : any
  public vendas_finalizadas : number = 0

  public estadoLoad : boolean = false
  public receitas_validas : boolean = false
  public msg_produtos_erro : string = "Você deve escolher as condições de pesquisa acima"
  public estadoForm : string = 'disabled'

  public formHistorico : FormGroup = new FormGroup({
    'status_receita' : new FormControl(null),
    'cpf' : new FormControl(null)
  })

  public cpfValido : boolean = false

  constructor(private validaCpf : ValidaCpf,
    private requests : Requests) { }

  ngOnInit() {
    this.setInfo()
  }

  setInfo(){
    this.formHistorico.get('status_receita').setValue("0")
  }

  public model1: any = {  };

  public model2: any = {  };

  public myDatePickerOptions1: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
  }

  public myDatePickerOptions2: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
  }

  onSubmit(form){
    this.mostrarLoader(true, "Buscando Receita")

    let d1
    let d2

    let id_profissional = this.id_profissional_escolhido
    if(form.value.data_inicio.formatted !== undefined){
      d1 = form.value.data_inicio.formatted;
      this.inicio = d1
     }else{
      d1 = '';
     }
     if(form.value.data_fim.formatted !== undefined){
      d2 = form.value.data_fim.formatted;
      this.fim = d2
     }else{
      d2 = '';
     }
     let separa1 = d1.split("/")
     let separa2 = d2.split("/")

     let ndata1 = separa1[2]+'-'+separa1[1]+'-'+separa1[0] + ' 00:00:00'
     let ndata2 = separa2[2]+'-'+separa2[1]+'-'+separa2[0] + ' 23:59:59'

     let dados_historico : any = {
       'cpf_profissional' : this.formHistorico.value.cpf,
       'status_receita' : this.formHistorico.value.status_receita,
       'data_inicio' : ndata1,
       'data_fim' : ndata2
     }

     console.log(dados_historico)

     this.requests.getHistoricoInterno(dados_historico).subscribe(
       res => {
        this.mostrarLoader(false, null)
        var dados : any = res
        if(dados.code===200){
          console.log(dados)
          this.lista_historicos = dados.data
          this.vendas_finalizadas = dados.venda
          this.mostrarHistorico(true, null)
        }else{
          this.mostrarHistorico(false, dados.mensagem)
        }
     });
  }

  mostrarHistorico(status, msg_error){
    this.receitas_validas = status
    this.msg_produtos_erro = msg_error
  }

  mostrarLoader(status: boolean, mensagem: string){
    this.estadoLoad = status
    this.mensagem_loader = mensagem
    if(!status)this.lista_historicos = null
  }

  ativarBotao(form){
    if(form.value.data_inicio.formatted!=undefined && form.value.data_fim.formatted!=undefined){
      this.estadoForm = ''
    }else{
      this.estadoForm = 'disabled'
    }
  }

  validaCPF(cpf){
    this.cpfValido = this.validaCpf.validarCPF(this.formHistorico.value.cpf)
  }

}
