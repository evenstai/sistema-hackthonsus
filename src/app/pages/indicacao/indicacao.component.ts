import { Component, OnInit } from '@angular/core';
import { Requests } from 'src/services/requests.service';
import { Constants } from 'src/utils/Constantes';
import { FormGroup, FormControl } from '@angular/forms';
import { ValidaCpf } from 'src/utils/valida-cpf';
import { TelefoneValidation } from 'src/utils/valida-telefone';
import { ResponsesModuleService } from 'src/services/modules_services/responses_module.service';
import { ReceitaService } from 'src/services/modules_services/receita.service';
//declare var $;
//declare let html2canvas;

@Component({
  selector: 'app-indicacao',
  templateUrl: './indicacao.component.html',
  styleUrls: ['./indicacao.component.css'],
  providers: [ValidaCpf,
    TelefoneValidation,
    ResponsesModuleService,
    ReceitaService]
})
export class IndicacaoComponent implements OnInit {
  public celular_mask = ['(', /\d/, /\d/,')', /\d/, /\d/,/\d/,/\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public cpf_mask = [/\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '-', /\d/, /\d/]
  public birth_mask = [/\d/, /\d/,'/', /\d/, /\d/, '/', /\d/,/\d/,/\d/,/\d/]

  public estadoLoadBusca : boolean = false
  public estadoLoad : boolean = false
  public responseScreen : boolean =  false
  public indicacaoScreen : boolean =  true
  public receitaScreen : boolean =  false

  public mensagem_loader : string
  public msg_erro_busca : string

  public codigo_receita : string = null

  public cliente : any
  public id_cliente : number
  public search_cpf : boolean

  public url_imagens : string = Constants.API_IMAGENS_PRODUTOS

  public formCliente :FormGroup = new FormGroup({
    'nome' : new FormControl(null),
    'cpf' : new FormControl(null),
    'endereco' : new FormControl(null),
    'email' : new FormControl(null),
    'telefone' : new FormControl(null),
    'data_nasc' : new FormControl(null),
    'numero' : new FormControl(null),
    'mensagem' : new FormControl(null)
  })

  public formObs : FormGroup = new FormGroup({
    'mensagem' : new FormControl(null)
  })

  public formBuscarProduto :FormGroup = new FormGroup({
    'nome' : new FormControl(null)
  })

  public outros_prod : any = null

  public produtos_selecionados = []
  public quant_prod_selecionados : number = 0

  public categorias : any
  public id_linha_médica : number = 6
  public prod_linha_medica : any
  public id_alimentar : number = 7
  public prod_alimentar : any
  public prod_mais_indicados : any = null

  public nomeValido : boolean = false
  public cpfValido : boolean = false
  public enderecoValido : boolean = false
  public emailValido : boolean = false
  public telefoneValido : boolean = false
  public data_nascValido : boolean = false

  public estadoForm : string = "disabled"

  constructor(
    private validaCpf : ValidaCpf,
    private validaTelef : TelefoneValidation,
    private requests : Requests,
    private responsesModuleService : ResponsesModuleService,
    private receitaService : ReceitaService) { }

  ngOnInit() {
    this.listarMedicamentosAlimentar()
    this.listarProdutosLinhaMedica()
    this.listarProdutosMaisIndicados()
  }

  setInfo() {
    if (this.cliente != undefined && this.cliente != null) {
      this.formCliente.get('nome').setValue(this.cliente.nome)
      this.formCliente.get('email').setValue(this.cliente.email)
      this.formCliente.get('telefone').setValue(this.cliente.celular)
      this.formCliente.get('endereco').setValue(this.cliente.endereco)
      this.formCliente.get('data_nasc').setValue(this.cliente.data_nascimento)
      this.formObs.get('mensagem').setValue("")

      this.id_cliente = this.cliente.id

      this.validaNome("")
      this.validaCPF(this.formCliente.get('cpf'))
      this.validaEmail("")
      this.validaData("")
      this.validaTelefone(this.formCliente.get('telefone'))
      this.validaEndereco("")
    }else{
      this.formCliente.get('nome').setValue("")
      this.formCliente.get('email').setValue("")
      this.formCliente.get('telefone').setValue("")
      this.formCliente.get('endereco').setValue("")
      this.formCliente.get('data_nasc').setValue("")
      this.formObs.get('mensagem').setValue("")

      this.cpfValido = false
      this.validaNome("")
      this.validaEmail("")
      this.validaTelefone(this.formCliente.get('telefone'))
      this.validaEndereco("")
      this.validaData("")
    }
  }

  fazerReceita(){
    this.mostrarLoader(true, "Fazendo receita")
    let dados_receita : any = {
      'produtos' : this.produtos_selecionados,
      'cpf_cliente' : this.formCliente.value.cpf,
      'nome' : this.formCliente.value.nome,
      'email' : this.formCliente.value.email,
      'celular' : this.formCliente.value.telefone,
      'data_nascimento' : this.formCliente.value.data_nasc,
      'endereco' : this.formCliente.value.endereco,
      'cep' : "",
      'numero' : "",
      'observacao' : this.formObs.value.mensagem
    }

    this.requests.fazerReceita(dados_receita).subscribe(
      res =>{
        this.mostrarLoader(false, null)
        var resp_receita : any = res
        console.log(resp_receita)
        if(resp_receita.code===200){
          this.codigo_receita = resp_receita.data.receita[0].codigo
          this.receitaService.enviaDadosReceita(resp_receita.data)
          this.mostrarTelaReceita()
          
        }else{
          this.codigo_receita = null
          this.mostrarResponseScreen(
            true,
            resp_receita.code,
            resp_receita.mensagem,
            "400px"
          )
        }
      }
    )
  }

  buscarCPF(){
    if(this.search_cpf){
      let dados_cliente : any = {
        'cpf' : this.formCliente.value.cpf
      }
      this.requests.buscarClienteCPF(dados_cliente).subscribe(
        res => {
          var resp_cliente : any = res
          console.log(resp_cliente)
          if(resp_cliente.code===200){
            this.cliente = resp_cliente.data[0]
            this.setInfo()
          }else{
            this.cliente = null
          }
        }
      )
    }
  }

  buscarProdutoViaInput(){
    if(this.formBuscarProduto.value.nome.length>=3){
      this.buscarProduto()
    }else{
      this.outros_prod = null
    }
  }

  buscarProduto(){
    this.mostrarLoaderBusca(true, "Buscando produtos");

      let dados_produtos : any = {
        'nome' : this.formBuscarProduto.value.nome,
        'categoria' : 0,
        'marca' : 0,
        'limit' : 20,
        'offset' : 0
      }
  
      this.requests.buscarProdutos(dados_produtos).subscribe(
        res => {
          this.mostrarLoaderBusca(false, null)
          var resp_busca : any = res
          console.log(resp_busca)
          if(resp_busca.code==200){
            this.outros_prod = resp_busca.data
            this.msg_erro_busca = null
          }else{
            this.outros_prod = null
            this.msg_erro_busca = resp_busca.mensagem;
          }
        }
      )
  }


  listarProdutosMaisIndicados(){
    let dados_produtos : any = {
      'limit' : 20,
      'offset' : 0
    }

    this.requests.listarProdutosMaisIndicados(dados_produtos).subscribe(
      res => {
        var resp_produtos : any = res
        if(resp_produtos.code===200){
          this.prod_mais_indicados = resp_produtos.data
        }else{
          this.prod_mais_indicados = null
        }
      }
    )
  }

  listarMedicamentosAlimentar(){
    let dados_produtos : any = {
      'id_categoria' : this.id_alimentar,
      'limit' : 20,
      'offset' : 0
    }

    this.requests.listarProdutosByCategoria(dados_produtos).subscribe(
      res => {
        var resp_produtos : any = res
        this.prod_alimentar = resp_produtos.data
      }
    )
  }

  listarProdutosLinhaMedica(){
    let dados_produtos : any = {
      'id_categoria' : this.id_linha_médica,
      'limit' : 20,
      'offset' : 0
    }

    this.requests.listarProdutosByCategoria(dados_produtos).subscribe(
      res => {
        var resp_produtos : any = res
        this.prod_linha_medica = resp_produtos.data
      }
    )
  }

  showDescricao(descricao){
    if(descricao!=null){
      return descricao.substr(0,90) + " ..."
    }else{
      return "Produto sem descrição."
    }
    
  }

  mudarStatusProdutoSelecionado(evento, produto){
    this.adicionaOuRemove(evento, produto)
    this.ativarBotao()
    this.quant_prod_selecionados = this.produtos_selecionados.length
  }

  adicionaOuRemove(status, produto) {
    console.log(status, produto.id);
    let index = this.produtos_selecionados.findIndex(produto_s => produto_s.id == produto.id);
    if(index < 0) {
          this.produtos_selecionados.push({id: produto.id});
    } else {
        this.produtos_selecionados.splice(index, 1);
    }
    console.log(this.produtos_selecionados)
  }

  validaNome(nome : string){
    if(this.formCliente.value.nome.length<7 || this.formCliente.value.nome.indexOf(" ") == -1){
      this.nomeValido = false  
    }else{
      this.nomeValido = true
    }
    this.ativarBotao()
  }

  validaCPF(cpf){
    this.cpfValido = this.validaCpf.validarCPF(this.formCliente.value.cpf)
    if(this.cpfValido){
      this.buscarCPF()
      this.search_cpf = false
    }else{
      this.cliente = null
      this.search_cpf = true
      this.setInfo()
    }
    this.ativarBotao()
  }


  validaEndereco(endereco : string){
    if(this.formCliente.value.endereco.length>5){
      this.enderecoValido = true
      
    }else{
      this.enderecoValido = false
    }
    this.ativarBotao()
  }

  validaEmail(email_teste: string){
    if(this.formCliente.value.email.indexOf('@') == -1 || this.formCliente.value.email.indexOf('.') == -1 ){
      this.emailValido = false
    }else{
      this.emailValido = true
    }
    this.ativarBotao()
  }

  validaTelefone(telefone){
    this.telefoneValido = this.validaTelef.validarTel(this.formCliente.value.telefone)
    this.ativarBotao()
  }

  validaData(data){
    var dt = this.formCliente.value.data_nasc.replace("/","").replace("/","")
    var dataFinal = dt.replace(/_/g,"")
    if(dataFinal.toString().length == 8){
      var dia = dataFinal.substring(0,2)
      var mes = dataFinal.substring(2,4)
      var ano = dataFinal.substring(4)
      var ano_atual = new Date
      var ano_atual_final = ano_atual.getFullYear()-18
      if(dia<=31 && mes<=12 && ano<=ano_atual_final && ano>1900){
        this.data_nascValido = true
      }else{
        this.data_nascValido = false
      }
    }else{
      this.data_nascValido = false
    }
    this.ativarBotao()
  }

  ativarBotao(){
    if(this.nomeValido && this.cpfValido && this.telefoneValido && this.produtos_selecionados.length>0){
      this.estadoForm = ''
    }else{
      this.estadoForm = 'disabled'
    }
  }

  mostrarResponseScreen(status: boolean, code: number, mensagem: string, altura: string){
    this.responseScreen = status
    this.responsesModuleService.enviaConfigResponse(code, mensagem, altura)
  }

  mostrarLoader(status: boolean, mensagem: string){
    this.estadoLoad = status
    this.mensagem_loader = mensagem
  }

  mostrarLoaderBusca(status: boolean, mensagem: string){
    this.estadoLoadBusca = status
    this.mensagem_loader = mensagem
  }

  novaReceita(){
    this.formCliente.reset()
    this.formObs.reset()
    this.codigo_receita = null
    this.produtos_selecionados = null
    this.quant_prod_selecionados = 0
    this.cliente = null
    this.setInfo()
    this.mostrarTelaIndicacao()
  }

  imprimirReceita(){
      var conteudo = document.getElementById('receita').innerHTML,
      tela_impressao = window.open('about:blank');

      tela_impressao.document.write(conteudo);
      tela_impressao.window.print();
      tela_impressao.window.close();

      /*
      html2canvas($('#receita').get(0)).then( function (canvas) {
        console.log(canvas);
        var myImage = canvas.toDataURL("image/png");
                var tWindow = window.open("");
                $(tWindow.document.body)
                    .html("<img id='Image' src=" + myImage + " style='width:100%; height: 100%;'></img>")
                    .ready(function() {
                        tWindow.focus();
                        tWindow.print();
                        tWindow.close();;
                    });
        });*/
  }

  mostrarTelaIndicacao(){
    this.indicacaoScreen = true
    this.receitaScreen = false
  }

  mostrarTelaReceita(){
    this.indicacaoScreen = false
    this.receitaScreen = true
  }

  mostrarImg(img : String){
    if(img!=null){
      return this.url_imagens+img
    }else{
      return "/assets/new_product_img.svg"
    }
  }

}
