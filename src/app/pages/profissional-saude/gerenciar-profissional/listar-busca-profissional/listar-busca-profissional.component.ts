import { Component, OnInit } from '@angular/core';
import { Constants } from 'src/utils/Constantes';
import { ListaBuscaProfissionalService } from 'src/services/modules_services/lista_busca_profissional.service';
import { EditProfissionalService } from 'src/services/modules_services/edit_profissional.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listar-busca-profissional',
  templateUrl: './listar-busca-profissional.component.html',
  styleUrls: ['./listar-busca-profissional.component.css'],
  providers: [ 
    ListaBuscaProfissionalService,
    EditProfissionalService ]
})
export class ListarBuscaProfissionalComponent implements OnInit {

  public profissionais : any = ListaBuscaProfissionalService.profissional

  public url_img_prof : string = Constants.API_IMAGENS_PROFISSIONAIS

  constructor(
    private editProfissionalService : EditProfissionalService,
    private rotas : Router) { }

  ngOnInit() {
  }

  mostrarImg(img : String){
    if(img!=null){
      return this.url_img_prof+img
    }else{
      return "/assets/new_user_img.svg"
    }
  }

  editarProfissional(profissional){
    this.editProfissionalService.enviaEditProfissional(profissional)
    this.rotas.navigate(['dashboard/editar-prof'])
  }

}
