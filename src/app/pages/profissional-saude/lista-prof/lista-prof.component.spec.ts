import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaProfComponent } from './lista-prof.component';

describe('ListaProfComponent', () => {
  let component: ListaProfComponent;
  let fixture: ComponentFixture<ListaProfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaProfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaProfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
