import { Component, OnInit } from '@angular/core';
import { TogglePasswordService } from 'src/services/modules_services/toggle_password.service';

@Component({
  selector: 'app-toggle-password',
  templateUrl: './toggle-password.component.html',
  styleUrls: ['./toggle-password.component.css'],
  providers: [ TogglePasswordService ]
})
export class TogglePasswordComponent implements OnInit{

  public lottieConfig: Object;
  private anim: any;
  
  private animationSpeed: number = 1;

  constructor() {
      this.lottieConfig = {
          path: 'assets/animation/eyepassword.json',
          autoplay: true,
          loop: false
      };
  }

  ngOnInit(){
    TogglePasswordService.emitirMostrarPassword.subscribe(
      mostrar => {
        if(mostrar){
          this.anim.playSegments([1,20], true)
        }else{
          this.anim.playSegments([20,40], true)
        }
      }
    )
  }
  
  handleAnimation(anim: any) {
    this.anim = anim;
    this.anim.setSpeed(1.5)
  }

  stop() {
      this.anim.stop();
  }

  play() {
      this.anim.play();
  }

  pause() {
      this.anim.pause();
  }

  setSpeed(speed: number) {
      this.animationSpeed = speed;
      this.anim.setSpeed(speed);
  }

}
