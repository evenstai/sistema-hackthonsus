export class Constants {


    constructor(){
        var API_URL : string
        var API_IMAGENS : string
    }

    //PROD
    static API_URL = 'https://apihackathonsus.herokuapp.com/sis_web/'

    static API_IMAGENS = 'https://api.saudemanauara.com:8080/'
    static API_IMAGENS_PRODUTOS = 'https://api.saudemanauara.com:8080/imagens/produtos/'
    static API_IMAGENS_PROFISSIONAIS = 'https://api.saudemanauara.com:8080/imagens/profissional/'
    
}