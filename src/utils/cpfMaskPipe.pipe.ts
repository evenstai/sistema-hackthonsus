import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: 'cpfMaskPipe'
})
export class CpfMaskPipe implements PipeTransform{
    transform(texto: string): string {
        if(texto.length===11){
            return texto.substr(0,3) 
            + '.' + texto.substr(3,3)
            + '.' + texto.substr(6,3)
            + '-' + texto.substr(9,2)
        }
        return texto
    }
}