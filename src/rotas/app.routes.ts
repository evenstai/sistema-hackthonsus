import { MeusDadosColabComponent } from './../app/pages/meus-dados-colab/meus-dados-colab.component';
import { ReceitaComponent } from './../app/pages/receita/receita.component';
import { MeusDadosComponent } from './../app/pages/meus-dados/meus-dados.component';
import { EditarProdutoComponent } from './../app/pages/produtos/editar-produto/editar-produto.component';
import { EditarClienteComponent } from './../app/pages/clientes/editar-cliente/editar-cliente.component';
import { FuncoesAdmComponent } from './../app/pages/funcoes-adm/funcoes-adm.component';
import { HistoricoProfissionalComponent } from './../app/pages/historico-profissional/historico-profissional.component';
import { IndicacaoComponent } from './../app/pages/indicacao/indicacao.component';
import { HistoricoComponent } from './../app/pages/historico/historico.component';
import { GerenciarComponent } from './../app/pages/clientes/gerenciar/gerenciar.component';
import { ClientesComponent } from './../app/pages/clientes/clientes.component';
import { EditarProfComponent } from './../app/pages/profissional-saude/editar-prof/editar-prof.component';
import { GerenciarProfissionalComponent } from './../app/pages/profissional-saude/gerenciar-profissional/gerenciar-profissional.component';
import { ProfissionalSaudeComponent } from './../app/pages/profissional-saude/profissional-saude.component';
import { GerenciarProdutoComponent } from './../app/pages/produtos/gerenciar-produto/gerenciar-produto.component';
import { EditarColaboradorComponent } from './../app/pages/funcoes-adm/editar-colaborador/editar-colaborador.component';
import { AuthGuard } from './../services/guard/auth-guard.service';
import { Routes, RouterModule } from '@angular/router'

import { AcessoComponent } from '../app/acesso/acesso.component'
import { VendasComponent } from '../app/pages/vendas/vendas.component'

import { AuthGuardIn } from '../services/guard/auth-in-guard.service'
import { PagesComponent } from '../app/pages/pages.component'
import { NgModule } from '@angular/core';
import { ProdutosComponent } from 'src/app/pages/produtos/produtos.component';
import { AcessoInternoComponent } from 'src/app/acesso-interno/acesso-interno.component';
import { FarmaciasComponent } from 'src/app/pages/farmacias/farmacias.component';
import { NutrirEstoqueComponent } from 'src/app/pages/nutrir-estoque/nutrir-estoque.component';

export const ROTAS: Routes = [

    { path: '', component: AcessoComponent, canActivate: [ AuthGuardIn ]},
    { path: 'adm', component: AcessoInternoComponent, canActivate: [ AuthGuardIn ]},
    { path: 'dashboard', component: PagesComponent, canActivate: [ AuthGuard ],
        children: [
            { path: '', component: VendasComponent, canActivate: [ AuthGuard ]},
            { path: 'vendas', component: VendasComponent, canActivate: [ AuthGuard ]},
            { path: 'produtos', component: ProdutosComponent, canActivate: [ AuthGuard ]},
            { path: 'gerenciar-produto', component: GerenciarProdutoComponent, canActivate: [ AuthGuard ]},
            { path: 'profissional-saude', component: ProfissionalSaudeComponent, canActivate: [ AuthGuard ]},
            { path: 'gerenciar-profissional', component: GerenciarProfissionalComponent, canActivate: [ AuthGuard ]},
            { path: 'editar-prof', component: EditarProfComponent, canActivate: [ AuthGuard ]},
            { path: 'clientes', component: ClientesComponent, canActivate: [ AuthGuard ]},
            { path: 'gerenciar', component: GerenciarComponent, canActivate: [ AuthGuard ]},
            { path: 'historico', component: HistoricoComponent, canActivate: [ AuthGuard ]},
            { path: 'indicacao', component: IndicacaoComponent, canActivate: [ AuthGuard ]},
            { path: 'historico-profissional', component: HistoricoProfissionalComponent, canActivate: [ AuthGuard ]},
            { path: 'funcoes-adm', component: FuncoesAdmComponent, canActivate: [ AuthGuard ]},
            { path: 'editar-colaborador', component: EditarColaboradorComponent, canActivate: [ AuthGuard ]},
            { path: 'editar-cliente', component: EditarClienteComponent, canActivate: [ AuthGuard ]},
            { path: 'editar-produto', component: EditarProdutoComponent, canActivate: [ AuthGuard ]},
            { path: 'meus-dados', component: MeusDadosComponent, canActivate: [ AuthGuard ]},
            { path: 'meus-dados-colab', component: MeusDadosColabComponent, canActivate: [ AuthGuard ]},
            { path: 'receita', component: ReceitaComponent, canActivate: [ AuthGuard ]},
            { path: 'farmacias', component: FarmaciasComponent, canActivate: [ AuthGuard ]},
            { path: 'nutrir_estoque', component: NutrirEstoqueComponent, canActivate: [ AuthGuard ]}
        ]}
    ]
@NgModule({
    imports: [RouterModule.forRoot(ROTAS, {useHash: true})],
    exports: [RouterModule]
  })
  export class AppRoutingModule { }
