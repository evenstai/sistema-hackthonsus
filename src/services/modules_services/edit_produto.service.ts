import { Injectable, EventEmitter  } from "@angular/core"

@Injectable()
export class EditProdutoService {

    static produto : any

    static emitirProduto = new EventEmitter<any>()

    enviaEditProduto(produto :any){
        EditProdutoService.produto = produto
    }

}