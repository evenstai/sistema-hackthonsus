import { Injectable, EventEmitter } from "@angular/core"

@Injectable()
export class ProdutosVendaService {

    static emitirProdutosVenda = new EventEmitter<any>()

    enviaProdutosVenda(dados_produtos_venda :any){
        ProdutosVendaService.emitirProdutosVenda.emit(dados_produtos_venda)
        console.log(dados_produtos_venda)
    }

}