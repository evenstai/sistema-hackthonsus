import { Injectable, EventEmitter  } from "@angular/core"

@Injectable()
export class EditOperadorService {

    static info_operador : any

    static emitirOperadorConfig = new EventEmitter<any>()

    enviaInfoOperadorConfig(info_operador :any){
        EditOperadorService.emitirOperadorConfig.emit(info_operador)
        EditOperadorService.info_operador = info_operador
    }

}