import { Injectable, EventEmitter } from "@angular/core"

@Injectable()
export class ImagemModuleService {

    static emitirImagemBase64 = new EventEmitter<String>()

    static imagem_base64 : string

    enviaImagemBase64(imagem_base64 :string){
        ImagemModuleService.imagem_base64 = imagem_base64
        ImagemModuleService.emitirImagemBase64.emit(imagem_base64)
    }

}