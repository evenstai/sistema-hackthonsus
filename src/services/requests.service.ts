import { Injectable } from "@angular/core"
import { HttpClient, HttpParams } from "@angular/common/http"
import { HttpHeaders } from '@angular/common/http'

import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpErrorHandler, HandleError } from './http-handle-error.service';
import { Transferencia } from "../models/transferencia.model";
import { Senhas } from "../models/senhas.model";
import { DadosAdmin } from "src/models/dados_admin.model";
import { AddColaborador } from "src/models/add_colaborador.model";
import { AddOperador } from "src/models/add_operador.model";
import { Constants } from "src/utils/Constantes";

@Injectable()
export class Requests {

  apiUrl = Constants.API_URL
  apiUrlImg = Constants.API_IMAGENS_PROFISSIONAIS
  apiUrlImgProfissionais = Constants.API_IMAGENS_PROFISSIONAIS
  apiUrlImgProdutos = Constants.API_IMAGENS_PRODUTOS

  //Endpoints
  buscar_receita = 'receita';
  fazer_receita = 'fazer_receita';
  finalizar_receita = 'finalizar_receita';
  add_profissional = 'add_profissional';
  atualizar_profissional = 'atualizar_profissional';
  profissionais = 'profissionais';
  buscar_profissionais = 'buscar_profissionais';
  add_produto = 'add_produto';
  produtos = 'produtos';
  produtos_categoria = 'produtos_categoria';
  buscar_produtos = 'buscar_produtos';
  add_categoria = 'add_categoria';
  listar_categorias = 'listar_categorias';
  produtos_mais_indicados = 'produtos_mais_indicados';
  add_marca = 'add_marca';
  listar_marcas = 'listar_marcas';
  add_colaborador = 'add_colaborador';
  atualizar_dados_colaborador = 'atualizar_dados_colaborador';
  atualizar_dados_profissional = 'atualizar_dados_profissional'; //endpoint de uso do profissional
  atualizar_senha_pessoal = 'atualizar_senha_pessoal';
  clientes = 'clientes';
  busca_cliente_cpf = 'busca_cliente_cpf';
  busca_cliente_nome = 'busca_cliente_nome';
  add_cliente = 'add_cliente';
  atualizar_cliente = 'atualizar_cliente';
  atualizar_status_produto_receita = 'atualizar_status_produto_receita';
  historico_interno = 'historico_interno';
  historico_profissional = 'historico_profissional';
  cadastrar_farmacia = 'cadastrar_farmacia';


  unidades = 'unidades_saldo';
  dados_conta = 'dados_conta';
  transferencia = 'sacar_saldo';
  colaboradores = 'colaboradores';
  //add_colaborador = 'add_colaborador';
  delete_colaborador = 'delete_colaborador';
  delete_operador = 'delete_operador';
  reenviar_convite = 'reenviar_convite';
  add_operador = 'cadastra_operador';
  last_operador = 'last_operador';
  ativar_operador = 'ativar_operador';
  last_deposito = 'last_deposito';
  get_proposta = 'get_proposta';
  depositar_reserva = 'depositar_reserva';
  mudar_senha_admin = 'mudar_senha_admin';
  mudar_dados_admin = 'mudar_dados_admin';
  mudar_dados_negocio = 'mudar_dados_negocio';
  mudar_imagem_negocio = 'mudar_imagem_negocio';
  get_mensalidades = 'get_mensalidades';
  agenda_estorno = 'agenda_estorno';
  lista_estorno = 'lista_estorno';
  img_matriz = 'img_matriz';
  lista_voucher = 'lista_voucher';
  lista_transferencias = 'lista_transferencia_voucher';
  dados_saldo = 'saldo_voucher';
  transferir_saldo = 'agenda_transferencia_voucher';
  calcular_valor = 'calcular_transferencia_voucher';

  inserir_notas = 'inserir_notas';

  auth_token = localStorage.getItem('auth_token');

  private handleError: HandleError;

  constructor(private http: HttpClient, httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('AuthService');
  }

  public getHttpOptions() {
    let auth_token = localStorage.getItem('auth_token');
    var httpOptions
    return httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': auth_token
      })
    };
  }

  public getUnidades(): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<string>(this.apiUrl + this.unidades, null, httpOptions)
      .pipe(
        catchError(this.handleError('getUnidades', null))
      )

  }

  //---------------------------- FARMACIAS -------------------------------------------------

  public insertFarmacia(dados_farmacia: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.cadastrar_farmacia, dados_farmacia, httpOptions)
      .pipe(
        catchError(this.handleError('insertFarmacia', null))
      )
  }

  //---------------------------- RECEITAS -------------------------------------------------

  public getReceita(dados_receita: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.buscar_receita, dados_receita, httpOptions)
      .pipe(
        catchError(this.handleError('getReceita', null))
      )
  }

  public fazerReceita(dados_receita: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.fazer_receita, dados_receita, httpOptions)
      .pipe(
        catchError(this.handleError('fazerReceita', null))
      )
  }

  public finalizarReceita(dados_receita: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.finalizar_receita, dados_receita, httpOptions)
      .pipe(
        catchError(this.handleError('finalizarReceita', null))
      )
  }

  public updateStatusProdutoReceita(dados_produto_receita: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.atualizar_status_produto_receita, dados_produto_receita, httpOptions)
      .pipe(
        catchError(this.handleError('updateStatusProdutoReceita', null))
      )
  }

  //---------------------------- HISTORICO -------------------------------------------------

  public getHistoricoInterno(dados_historico: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.historico_interno, dados_historico, httpOptions)
      .pipe(
        catchError(this.handleError('getHistoricoInterno', null))
      )
  }

  public getHistoricoProfissional(dados_historico: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.historico_profissional, dados_historico, httpOptions)
      .pipe(
        catchError(this.handleError('getHistoricoProfissional', null))
      )
  }

  //---------------------------- COLABORADOR -------------------------------------------------

  public insertColaborador(dados_colaborador: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.add_colaborador, dados_colaborador, httpOptions)
      .pipe(
        catchError(this.handleError('insertColaborador', null))
      )
  }

  //---------------------------- DADOS -------------------------------------------------

  public atualizarDadosPessoaisColaborador(dados_pessoais: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.atualizar_dados_colaborador, dados_pessoais, httpOptions)
      .pipe(
        catchError(this.handleError('atualizarDadosPessoais', null))
      )
  }

  public atualizarDadosPessoaisProfissional(dados_pessoais: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.atualizar_dados_profissional, dados_pessoais, httpOptions)
      .pipe(
        catchError(this.handleError('atualizarDadosPessoaisProfissional', null))
      )
  }

  public atualizarSenhaPessoal(dados_senha: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.atualizar_senha_pessoal, dados_senha, httpOptions)
      .pipe(
        catchError(this.handleError('atualizarSenhaPessoal', null))
      )
  }

  //---------------------------- PROFISSIONAL DE SÁUDE -------------------------------------------------

  public cadastrarProfissional(dados_profissional: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.add_profissional, dados_profissional, httpOptions)
      .pipe(
        catchError(this.handleError('cadastrarProfissional', null))
      )
  }

  public atualizarProfissional(dados_profissional: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.atualizar_profissional, dados_profissional, httpOptions)
      .pipe(
        catchError(this.handleError('atualizarProfissional', null))
      )
  }

  public listarProfissionais(dados_profissionais: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.profissionais, dados_profissionais, httpOptions)
      .pipe(
        catchError(this.handleError('listarProfissionais', null))
      )
  }

  public buscarProfissionais(dados_profissionais: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.buscar_profissionais, dados_profissionais, httpOptions)
      .pipe(
        catchError(this.handleError('buscarProfissionais', null))
      )
  }

  //---------------------------- CLIENTES -------------------------------------------------

  public cadastrarCliente(dados_cliente: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.add_cliente, dados_cliente, httpOptions)
      .pipe(
        catchError(this.handleError('cadastrarCliente', null))
      )
  }

  public atualizarCliente(dados_cliente: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.atualizar_cliente, dados_cliente, httpOptions)
      .pipe(
        catchError(this.handleError('atualizarCliente', null))
      )
  }

  public listarCliente(dados_clientes: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.clientes, dados_clientes, httpOptions)
      .pipe(
        catchError(this.handleError('listarCliente', null))
      )
  }

  public buscarClienteCPF(dados_cliente: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.busca_cliente_cpf, dados_cliente, httpOptions)
      .pipe(
        catchError(this.handleError('buscarClienteCPF', null))
      )
  }

  public buscarClienteNome(dados_cliente: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.busca_cliente_nome, dados_cliente, httpOptions)
      .pipe(
        catchError(this.handleError('buscarClienteNome', null))
      )
  }

  //---------------------------- PRODUTOS -------------------------------------------------

  public cadastrarProduto(dados_produto: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.add_produto, dados_produto, httpOptions)
      .pipe(
        catchError(this.handleError('cadastrarProduto', null))
      )
  }

  public listarProdutos(dados_produtos: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.produtos, dados_produtos, httpOptions)
      .pipe(
        catchError(this.handleError('listarProdutos', null))
      )
  }
  public listarProdutosMaisIndicados(dados_produtos: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.produtos_mais_indicados, dados_produtos, httpOptions)
      .pipe(
        catchError(this.handleError('listarProdutosMaisIndicados', null))
      )
  }

  public listarProdutosByCategoria(dados_produtos: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.produtos_categoria, dados_produtos, httpOptions)
      .pipe(
        catchError(this.handleError('listarProdutosByCategoria', null))
      )
  }

  public buscarProdutos(dados_produtos: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.buscar_produtos, dados_produtos, httpOptions)
      .pipe(
        catchError(this.handleError('buscarProdutos', null))
      )
  }

  //---------------------------- CATEGORIA -------------------------------------------------

  public cadastrarCategoria(dados_categoria: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.add_categoria, dados_categoria, httpOptions)
      .pipe(
        catchError(this.handleError('cadastrarCategoria', null))
      )
  }

  public listarCategorias(): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.get(this.apiUrl + this.listar_categorias, httpOptions)
      .pipe(
        catchError(this.handleError('listarCategorias', null))
      )
  }

  //---------------------------- MARCA -------------------------------------------------

  public cadastrarMarca(dados_marca: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.add_marca, dados_marca, httpOptions)
      .pipe(
        catchError(this.handleError('cadastrarMarca', null))
      )
  }

  public listarMarcas(): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.get(this.apiUrl + this.listar_marcas, httpOptions)
      .pipe(
        catchError(this.handleError('listarMarcas', null))
      )
  }

  //---------------------------- FUNCÕES ADMINISTRATIVAS ----------------------------------

  public solicitarListaColaboradores(): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.get(this.apiUrl + this.colaboradores, httpOptions)
      .pipe(
        catchError(this.handleError('solicitarListaColaboradores', null))
      )
  }

  public cadastrarColaborador(add_colaborador: AddColaborador): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<AddColaborador>(this.apiUrl + this.add_colaborador, add_colaborador, httpOptions)
      .pipe(
        catchError(this.handleError('cadastrarColaborador', null))
      )
  }

  public excluirColaborador(delete_colaborador: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.delete_colaborador, delete_colaborador, httpOptions)
      .pipe(
        catchError(this.handleError('excluirColaborador', null))
      )
  }

  public reenviarConvite(cpf_colaborador: string): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post(this.apiUrl + this.reenviar_convite, { cpf_colaborador: cpf_colaborador }, httpOptions)
      .pipe(
        catchError(this.handleError('reenviarConvite', null))
      )
  }

  //---------------------------- PROPOSTA MENSALIDADE ----------------------------------

  public buscarProposta(proposta): Observable<string> {
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': ""
      })
    }
    return this.http.post(this.apiUrl + this.get_proposta, proposta, httpOptions)
      .pipe(
        catchError(this.handleError('buscarProposta', null))
      )

  }

  //---------------------------- DEPÓSITO DE RESERVA ----------------------------------

  public solicitarUltimoDeposito(id_unidade: string): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post(this.apiUrl + this.last_deposito, { id_unidade }, httpOptions)
      .pipe(
        catchError(this.handleError('solicitarUltimoDeposito', null))
      )

  }

  public depositarReservaDeTroco(deposito: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.depositar_reserva, deposito, httpOptions)
      .pipe(
        catchError(this.handleError('solicitarDeposito', null))
      )

  }

  //---------------------------- OPERADORES DE CAIXA ----------------------------------

  public cadastrarOperador(add_operador: AddOperador): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<AddOperador>(this.apiUrl + this.add_operador, add_operador, httpOptions)
      .pipe(
        catchError(this.handleError('cadastrarOperador', null))
      )
  }

  public solicitarUltimoOperador(): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.get(this.apiUrl + this.last_operador, httpOptions)
      .pipe(
        catchError(this.handleError('solicitarUltimoOperador', null))
      )

  }

  public ativarOperador(dados_ativar: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.ativar_operador, dados_ativar, httpOptions)
      .pipe(
        catchError(this.handleError('ativarOperador', null))
      )
  }

  public excluirOperador(delete_operador: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.delete_operador, delete_operador, httpOptions)
      .pipe(
        catchError(this.handleError('excluirOperador', null))
      )
  }

  //---------------------------- GERENCIA -------------------------------------------------

  public solicitarDadosConta(): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.get(this.apiUrl + this.dados_conta, httpOptions)
      .pipe(
        catchError(this.handleError('solicitarDadosConta', null))
      )

  }

  public solicitarTransferencia(transferencia: Transferencia): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<Transferencia>(this.apiUrl + this.transferencia, transferencia, httpOptions)
      .pipe(
        catchError(this.handleError('solicitarTransferencia', null))
      )

  }

  public mudarSenhaAdmin(senhas: Senhas): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<Senhas>(this.apiUrl + this.mudar_senha_admin, senhas, httpOptions)
      .pipe(
        catchError(this.handleError('mudarSenhaAdmin', null))
      )
  }

  public mudarDadosAdmin(dados_admin: DadosAdmin): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<DadosAdmin>(this.apiUrl + this.mudar_dados_admin, dados_admin, httpOptions)
      .pipe(
        catchError(this.handleError('mudarDadosAdmin', null))
      )
  }

  public mudarImagemNegocio(dados_imagem: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.mudar_imagem_negocio, dados_imagem, httpOptions)
      .pipe(
        catchError(this.handleError('mudarImagemNegocio', null))
      )
  }


  //---------------------------- MENSALIDADE -------------------------------------------------

  public solicitarFaturasMensalidade(id_unidade: string): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post(this.apiUrl + this.get_mensalidades, { id_unidade: id_unidade }, httpOptions)
      .pipe(
        catchError(this.handleError('solicitarFaturasMensalidade', null))
      )
  }

  //---------------------------- UNIDADES -------------------------------------------------

  public solicitarImgMatriz(): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.get(this.apiUrl + this.img_matriz, httpOptions)
      .pipe(
        catchError(this.handleError('solicitarImgMatriz', null))
      )
  }

  //---------------------------- ESTORNO -------------------------------------------------

  public solicitarEstorno(dados_estorno: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.agenda_estorno, dados_estorno, httpOptions)
      .pipe(
        catchError(this.handleError('solicitarEstorno', null))
      )
  }

  public listarEstorno(): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.get(this.apiUrl + this.lista_estorno, httpOptions)
      .pipe(
        catchError(this.handleError('listarEstorno', null))
      )
  }

  //---------------------------- VOUCHER -------------------------------------------------

  public listarVoucher(dados_voucher: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.lista_voucher, dados_voucher, httpOptions)
      .pipe(
        catchError(this.handleError('listarVoucher', null))
      )
  }

  public listarTransferencias(dados_transferencia: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.lista_transferencias, dados_transferencia, httpOptions)
      .pipe(
        catchError(this.handleError('listarTransferencias', null))
      )
  }

  public saldoVoucher(id_unidade: number): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post(this.apiUrl + this.dados_saldo, { id_unidade: id_unidade }, httpOptions)
      .pipe(
        catchError(this.handleError('saldoVoucher', null))
      )
  }

  public tranferirSaldo(transf: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.transferir_saldo, transf, httpOptions)
      .pipe(
        catchError(this.handleError('transferirSaldo', null))
      )
  }

  public calcularValor(calc: any): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post<any>(this.apiUrl + this.calcular_valor, calc, httpOptions)
      .pipe(
        catchError(this.handleError('calcularValorTransferencia', null))
      )

  }

  //---------------------------- Nota Fiscal -------------------------------------------------

  public insertNota(dados_nota): Observable<string> {
    var httpOptions = this.getHttpOptions()
    return this.http.post(this.apiUrl + this.inserir_notas, httpOptions)
      .pipe(
        catchError(this.handleError('inserir_notas', null))
      )
  }
}
