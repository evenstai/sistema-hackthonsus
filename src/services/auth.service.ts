import { Cadastro } from "src/models/cadastro.model";

import { Injectable } from "@angular/core"
import { HttpClient } from "@angular/common/http"
import { HttpHeaders } from '@angular/common/http'

import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpErrorHandler, HandleError } from './http-handle-error.service';
import { Constants } from "src/utils/Constantes";

const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
     // 'Authorization': ''
    })
  };

@Injectable()
export class Auth {

    apiUrl = Constants.API_URL
    apiUrlImg = Constants.API_IMAGENS_PROFISSIONAIS

    //Endpoints
    login = 'login';
    logout = 'logout';
    cadastro_empresa = 'cadastro_empresa';

    private handleError: HandleError;

    public getHttpOptions(){
        let auth_token = localStorage.getItem('auth_token');
        var httpOptions
        return httpOptions = {
            headers: new HttpHeaders({
                'Content-Type':  'application/json',
                'Authorization': auth_token
            })
          };
    }

    constructor(private http: HttpClient, httpErrorHandler: HttpErrorHandler){
        this.handleError = httpErrorHandler.createHandleError('AuthService');
    }

    public cadastrarEmpresa(cadastro: Cadastro):Observable<Cadastro> {
        return this.http.post<Cadastro>(this.apiUrl+this.cadastro_empresa, cadastro, httpOptions)
        .pipe(
            catchError(this.handleError('cadastrarEmpresa', null))
        )

    }

    public loginPL(cpf: string, senha: string): Observable<string>{

        var tipo = "PL";
        
            return this.http.post(this.apiUrl+this.login, {tipo, cpf, senha})
            .pipe(
                catchError(this.handleError('loginPL', null))
            )
    }

    public loginProfissional(cpf: string, senha: string): Observable<string>{

        var tipo = 2;
        
            return this.http.post(this.apiUrl+this.login, {tipo, cpf, senha})
            .pipe(
                catchError(this.handleError('loginProfissional', null))
            )
    }

    public loginPJ(cpf: string, senha: string): Observable<string>{

        var tipo = "PJ";
        
            return this.http.post(this.apiUrl+this.login, {tipo, cpf, senha})
            .pipe(
                catchError(this.handleError('loginPJ', null))
            )
    }

    public loginColaborador(cpf: string, senha: string): Observable<string>{

        var tipo = 1;
        
            return this.http.post(this.apiUrl+this.login, {tipo, cpf, senha})
            .pipe(
                catchError(this.handleError('loginColaborador', null))
            )
    }

    public logoutSisWeb():Observable<String> {
        
        var httpOptions = this.getHttpOptions()
        return this.http.post(this.apiUrl+this.logout, null, httpOptions)
        .pipe(
            catchError(this.handleError('logout', null))
        )
    }

}