import { TestBed, inject } from '@angular/core/testing';

import { InserviceService } from './inservice.service';

describe('InserviceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InserviceService]
    });
  });

  it('should be created', inject([InserviceService], (service: InserviceService) => {
    expect(service).toBeTruthy();
  }));
});
